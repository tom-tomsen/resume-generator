ImageName := cmvzdw1llwj1awxkzxi
Tag := latest

.PHONY: all
all: clean lint test unit-test-coverage

#
# lint
#
.PHONY: lint
lint: lint-composer lint-php lint-editorconfig

#
# lint-composer
#
.PHONY: lint-composer
lint-composer: composer-validate composer-normalize

.PHONY: composer-validate
composer-validate:
	@./tools/composer.phar validate

.PHONY: composer-normalize
composer-normalize:
	@./tools/composer-normalize --dry-run

#
# lint-php
#
.PHONY: lint-php
lint-php: php-cs phpstan phan psalm

.PHONY: php-cs
php-cs:
	@./tools/php-cs-fixer fix --diff

.PHONY: phan
phan:
	@./tools/phan \
		--no-progress-bar \
		-j`nproc` \
		-k .phan/config.php \
		-C

.PHONY: psalm
psalm:
	@mkdir -p ./build/test-results
	@./tools/psalm \
		--no-progress \
		--threads=`nproc`

.PHONY: phpstan
phpstan:
	@./tools/phpstan \
		analyse \
			--no-progress \
			--no-interaction \
			--level=max \
			src \
			tests \
			bin/generate

#
# lint-editorconfig
#
.PHONY: lint-editorconfig
lint-editorconfig:
	@./tools/editorconfig-checker \
		-exclude "\.git|tools|vendor|.*\.xml|.*\.cache|build|src/Converter/PDF/fonts/unifont"

#
# test
#
.PHONY: test
test: unit-test infection-test

#
# unit-test
#
.PHONY: unit-test
unit-test:
	@mkdir -p ./build/test-results
	./tools/phpunit \
		--log-junit ./build/test-results/unit.xml

.PHONY: unit-test-coverage
unit-test-coverage:
	@mkdir -p ./build/test-coverage
	@XDEBUG_MODE=coverage ./tools/php-with-xdebug ./tools/phpunit \
		--coverage-html ./build/test-coverage

#
# infection-test
#
.PHONY: infection-test
infection-test:

	@XDEBUG_MODE=coverage ./tools/infection \
		--initial-tests-php-options="-d zend_extension=$(shell find "/usr/local/lib/php" -name xdebug.so)" \
		-j`nproc` \
		run \
			--only-covered

#
# build
#
.PHONY: build
build: build-prod build-dev

.PHONY: build-prod
build-prod:
	docker \
		build \
			--pull \
			--tag tomtomsen/$(ImageName):latest \
			.

.PHONY: build-dev
build-dev:
	docker \
		build \
			--tag tomtomsen/$(ImageName):dev \
			--file Dockerfile.dev \
			.

#
# ship
#
.PHONY: ship
ship: docker-login ship-prod ship-build

.PHONY: docker-login
docker-login:
	docker login

.PHONY: ship-prod
ship-prod: build-prod
	docker push tomtomsen/$(ImageName):latest

.PHONY: ship-build
ship-build: build-dev
	docker tag tomtomsen/$(ImageName):dev tomtomsen/$(ImageName):build
	docker push tomtomsen/$(ImageName):build

#
# up
#
.PHONY: up
up: build-dev
	docker \
		run \
			--rm \
			-ti \
			-v ${PWD}:${PWD} \
			-w ${PWD} \
			--entrypoint sh \
			tomtomsen/$(ImageName):dev

#
# update-tools
#
.PHONY: update-tools
update-tools: update-tool-phive update-tool-composer

.PHONY: update-tool-phive
update-tool-phive:
	@./tools/phive.phar selfupdate

.PHONY: update-tool-composer
update-tool-composer:
	@./tools/composer.phar self-update

#
# update library
#
.PHONY: update-libs
update-libs: update-lib-phive update-lib-composer

.PHONY: update-lib-phive
update-lib-phive:
	@./tools/phive.phar update

.PHONY: update-lib-composer
update-lib-composer:
	@./tools/composer.phar update

#
# clean
#
.PHONY: clean
clean:
	@rm -rf ./build
	@find ./src/Converter/PDF/fonts/unifont/ -type f -not -name *.ttf -not -name ttfonts.php -exec rm {} +

#
# preview
#
.PHONY: preview
preview: generate
	magick convert -background white -alpha remove -alpha off "build/resume.pdf" "build/preview.png"
