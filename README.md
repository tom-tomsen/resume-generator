![tom-tomsen/resume-generator](https://bitbucket.org/tom-tomsen/resume-generator-bak/raw/readme/resources/resume-example.png "Resume Generator")

# Resume Generator

A resume generator to build my resume in different formats. Also, a simple project to play and experiment.

## Installing / Getting started

via composer

```
  composer require tomtomsen/resume-builder
  ./vendor/bin/generate [-l|--locale [LOCALE]] [--] [<input>] [<output>]
```

via docker

```
  docker pull tomtomsen/resume-generator:latest
  docker run --rm -ti tomtomsen/resume-generator:latest [-l|--locale [LOCALE]] [--] [<input>] [<output>]
```

## Usage 

```
  generate [-l|--locale [LOCALE]] [--] [<input>] [<output>]
```

### Arguments:

- `input`: resume data file
- `output`: output file

### Options:

- `-l, --locale[=LOCALE]`: locale [default: `de`]

## Developing

via local environment

```
  git clone https://bitbucket.org/tom-tomsen/resume-generator.git
  cd resume-generator
  composer install
```

via docker

```
  git clone https://bitbucket.org/tom-tomsen/resume-generator.git
  cd resume-generator
  make up
```

> Checkout Makefile for useful commands

### Building

To build docker images

```
  make build
```

#### Bitbucket pipeline

Uses the following environment variables:

- **SNYK_TOKEN**  
  Snyk helps you use open source and stay secure. Continuously find and fix vulnerabilities for npm, Maven, NuGet, RubyGems, PyPI and much more.

## Configuration

- **Date format**  
  see `resources/locales/*.php`

- **Image**  
  see `resources/resume.json` in `$.person.image`

## License

See LICENSE.md
