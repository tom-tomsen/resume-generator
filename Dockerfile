# - stage: builder ------------------------------------------------------------

FROM php:8-cli-alpine as builder

COPY bin/ /tmp/data/bin
COPY resources/ /tmp/data/resources
COPY src/ /tmp/data/src
COPY tools/composer.phar /tmp/data/tools/composer.phar
COPY composer.json /tmp/data/composer.json

WORKDIR /tmp/data

RUN apk add --no-cache \
	zip \
&& \
	tools/composer.phar install --no-dev --no-interaction --ansi

# - image ---------------------------------------------------------------------

FROM php:8-cli-alpine

ARG UID=1000
ARG GID=1000

RUN \
#
# libzip: php-ext: zlib
#
	apk add --no-cache \
		libzip-dev \
&& \
#
# zlib: fpdf (compressed pdf)
#
	docker-php-ext-install \
		zip \
&& \
	cp ${PHP_INI_DIR}/php.ini-production ${PHP_INI_DIR}/php.ini \
&& \
	sed -i "s/memory_limit = .*/memory_limit = -1/" ${PHP_INI_DIR}/php.ini && \
	sed -i "s/;assert.exception = .*/assert.exception = On/" ${PHP_INI_DIR}/php.ini && \
	sed -i "s/;assert.active = .*/assert.active = On/" ${PHP_INI_DIR}/php.ini && \
	sed -i "s/zend.assertions = .*/zend.assertions = 1/" ${PHP_INI_DIR}/php.ini \
&& \
	addgroup -g ${GID} builder \
&& \
	adduser -D -h /data -u ${UID} -G builder builder

USER builder

COPY --chown=builder:builder --from=builder /tmp/data /data

WORKDIR /data

ENTRYPOINT ["php", "bin/generate"]
