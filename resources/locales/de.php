<?php

declare(strict_types=1);

return [
	'date-format' => '{F} Y',
	'i18n' => [
		'Contact' => 'Kontakt',
		'Education' => 'Ausbildung',
		'Work Experience' => 'Berufserfahrung',
		'Certificates' => 'Zertifizierungen',
		'since {start}' => 'seit {start}',
		'{start} - {end}' => '{start} - {end}',
		'in {date}' => 'im {date}',
		'January' => 'Jänner',
		'February' => 'Februar',
		'March' => 'März',
		'April' => 'April',
		'May' => 'Mai',
		'June' => 'Juni',
		'July' => 'July',
		'August' => 'August',
		'September' => 'September',
		'October' => 'Oktober',
		'November' => 'November',
		'December' => 'Dezember',
	],
];
