<?php

declare(strict_types=1);

return [
	'date-format' => '{F} Y',
	'i18n' => [
		'Contact' => 'Contact',
		'Education' => 'Education',
		'Work Experience' => 'Work Experience',
		'Certificates' => 'Certificates',
		'since {start}' => 'since {start}',
		'{start} - {end}' => '{start} - {end}',
		'in {date}' => 'in {date}',
		'January' => 'January',
		'February' => 'February',
		'March' => 'March',
		'April' => 'April',
		'May' => 'May',
		'June' => 'June',
		'July' => 'July',
		'August' => 'August',
		'September' => 'September',
		'October' => 'October',
		'November' => 'November',
		'December' => 'December',
	],
];
