<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Formatter\DurationFormatter;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Formatter\DurationFormatter;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;
use tomtomsen\ResumeGenerator\Translator\NoTranslator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Formatter\DurationFormatter
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Formatter\DurationFormatter
 *
 * @uses \tomtomsen\ResumeGenerator\Formatter\DateFormatter
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Translator\NoTranslator
 *
 * @internal
 *
 * @small
 */
final class FormatSameDateDurationTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::format
	 * @covers ::t
	 */
	public function formatGerman(): void
	{
		$durationFormatter = new DurationFormatter(
			new DateFormatter('F Y', new NoTranslator()),
			new NoTranslator()
		);
		$date = Date::fromString('01.03.2000', 'd.m.Y');

		$duration = new Duration($date, $date);

		self::assertSame('in March 2000', $durationFormatter->format($duration));
	}
}
