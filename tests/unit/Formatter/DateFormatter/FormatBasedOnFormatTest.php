<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Formatter\DateFormatter;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Translator\NoTranslator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Formatter\DateFormatter
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Formatter\DateFormatter
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Translator\NoTranslator
 *
 * @internal
 *
 * @small
 */
final class FormatBasedOnFormatTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::format
	 *
	 * @dataProvider formatDataProvider
	 */
	public function formatAsExpected(string $format, string $expectedResult): void
	{
		$date = new Date(new DateTimeImmutable('01.03.2000'));

		$dateFormatter = new DateFormatter($format, new NoTranslator());

		self::assertSame($expectedResult, $dateFormatter->format($date));
	}

	/**
	 * @return array<array<int, string>>
	 */
	public function formatDataProvider(): array
	{
		return [
			['F Y', 'March 2000'],
			['{F} Y', 'March 2000'],
			['d.m.Y', '01.03.2000'],
		];
	}
}
