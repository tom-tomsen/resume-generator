<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Formatter\DateFormatter;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Translator\ArrayTranslator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Formatter\DateFormatter
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Formatter\DateFormatter
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Translator\ArrayTranslator
 *
 * @internal
 *
 * @small
 */
final class TranslateMonthNamesTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::format
	 *
	 * @dataProvider monthDataProvider
	 */
	public function formatAsExpected(string $dateStr, string $month): void
	{
		$date = new Date(new DateTimeImmutable($dateStr));

		$dateFormatter = new DateFormatter('{F} Y', new ArrayTranslator([
			$month => 'xxx',
		]));

		self::assertSame('xxx 2000', $dateFormatter->format($date));
	}

	/**
	 * @return array<int, array<int, string>>
	 */
	public function monthDataProvider(): array
	{
		return [
			['01.01.2000', 'January'],
			['01.02.2000', 'February'],
			['01.03.2000', 'March'],
			['01.04.2000', 'April'],
			['01.05.2000', 'May'],
			['01.06.2000', 'June'],
			['01.07.2000', 'July'],
			['01.08.2000', 'August'],
			['01.09.2000', 'September'],
			['01.10.2000', 'October'],
			['01.11.2000', 'November'],
			['01.12.2000', 'December'],
		];
	}
}
