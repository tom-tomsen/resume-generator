<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Reader\JSON\Reader;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SplFileInfo;
use tomtomsen\ResumeGenerator\Reader\JSON\Reader;
use function file_get_contents;
use function json_encode;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Reader\JSON\Reader
 *
 * @internal
 *
 * @small
 */
final class InvalidJSONTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::read
	 */
	public function notAJson(): void
	{
		$file = $this->setupFile('invalid.json', '<xml></xml>');
		$reader = new Reader($file);

		$this->expectException(RuntimeException::class);
		// TODO(tomtomsen): expect exception msg
		$reader->read();
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 */
	public function fileDoesNotExist(): void
	{
		$file = new SplFileInfo('unknown.json');

		$this->expectException(RuntimeException::class);
		$reader = new Reader($file);
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 */
	public function noReadPermission(): void
	{
		$file = $this->setupFile('invalid.json', $this->validJson(), 0333);

		$this->expectException(RuntimeException::class);
		$reader = new Reader($file);
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::read
	 */
	public function jsonTooDeep(): void
	{
		$a['a'] = 'a';

		for ($i = 0; 511 > $i; ++$i) {
			$a['a'] = $a;
		}
		$content = json_encode($a, 0, 512);
		self::assertNotFalse($content);
		$file = $this->setupFile('valid.json', $content);
		$reader = new Reader($file);
		$this->expectException(RuntimeException::class);
		$reader->read();
	}

	private function setupFile(string $filename, string $content, int $permission = 0644): SplFileInfo
	{
		$root = vfsStream::setup();
		$file = vfsStream::newFile($filename, $permission)
			->withContent($content)
			->at($root);

		return new SplFileInfo($file->url());
	}

	private function validJson(): string
	{
		$content = file_get_contents(__DIR__ . '/valid.json');
		self::assertNotFalse($content);

		return $content;
	}
}
