<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Reader\JSON\Reader;

use PHPUnit\Framework\TestCase;
use SplFileInfo;
use tomtomsen\ResumeGenerator\Reader\JSON\Reader;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Reader\JSON\Reader
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Reader\JSON\Reader
 *
 * @uses ::__construct
 * @uses \tomtomsen\ResumeGenerator\Models\Resume
 * @uses \tomtomsen\ResumeGenerator\Models\Person
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Contact
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Image
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 *
 * @internal
 *
 * @small
 */
final class CreatesResumeTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::parse
	 * @covers ::parsePerson
	 * @covers ::read
	 *
	 * @uses ::parseCertificate
	 * @uses ::parseEducation
	 * @uses ::parseExperience
	 * @uses ::parseInstitution
	 * @uses ::parseOrganisation
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Date
	 * @uses \tomtomsen\ResumeGenerator\Models\Duration
	 * @uses \tomtomsen\ResumeGenerator\Models\Education
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
	 * @uses \tomtomsen\ResumeGenerator\Models\Location
	 * @uses \tomtomsen\ResumeGenerator\Models\Url
	 * @uses \tomtomsen\ResumeGenerator\Reader\JSON\Reader
	 */
	public function personNameIsRight(): void
	{
		$reader = new Reader(new SplFileInfo(__DIR__ . '/valid.json'));
		$resume = $reader->read();

		self::assertSame('tom tomsen', (string) $resume->person()->name());
	}

	/**
	 * @test
	 *
	 * @covers ::parse
	 * @covers ::parsePerson
	 * @covers ::read
	 *
	 * @uses ::parseCertificate
	 * @uses ::parseEducation
	 * @uses ::parseExperience
	 * @uses ::parseInstitution
	 * @uses ::parseOrganisation
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Date
	 * @uses \tomtomsen\ResumeGenerator\Models\Duration
	 * @uses \tomtomsen\ResumeGenerator\Models\Education
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
	 * @uses \tomtomsen\ResumeGenerator\Models\Location
	 * @uses \tomtomsen\ResumeGenerator\Models\Url
	 * @uses \tomtomsen\ResumeGenerator\Reader\JSON\Reader
	 */
	public function personTitleIsRight(): void
	{
		$reader = new Reader(new SplFileInfo(__DIR__ . '/valid.json'));
		$resume = $reader->read();

		self::assertSame('open source developer', (string) $resume->person()->title());
	}

	/**
	 * @test
	 *
	 * @covers ::parse
	 * @covers ::parsePerson
	 * @covers ::read
	 *
	 * @uses ::parseCertificate
	 * @uses ::parseDuration
	 * @uses ::parseEducation
	 * @uses ::parseExperience
	 * @uses ::parseInstitution
	 * @uses ::parseOrganisation
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Date
	 * @uses \tomtomsen\ResumeGenerator\Models\Duration
	 * @uses \tomtomsen\ResumeGenerator\Models\Education
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
	 * @uses \tomtomsen\ResumeGenerator\Models\Location
	 * @uses \tomtomsen\ResumeGenerator\Models\Url
	 * @uses \tomtomsen\ResumeGenerator\Reader\JSON\Reader
	 */
	public function personDescriptionIsRight(): void
	{
		$reader = new Reader(new SplFileInfo(__DIR__ . '/valid.json'));
		$resume = $reader->read();

		self::assertSame('this is a description', (string) $resume->person()->description());
	}

	/**
	 * @test
	 *
	 * @covers ::parse
	 * @covers ::parsePerson
	 * @covers ::read
	 *
	 * @uses ::parseCertificate
	 * @uses ::parseDuration
	 * @uses ::parseEducation
	 * @uses ::parseExperience
	 * @uses ::parseInstitution
	 * @uses ::parseOrganisation
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Date
	 * @uses \tomtomsen\ResumeGenerator\Models\Duration
	 * @uses \tomtomsen\ResumeGenerator\Models\Education
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
	 * @uses \tomtomsen\ResumeGenerator\Models\Location
	 * @uses \tomtomsen\ResumeGenerator\Models\Url
	 * @uses \tomtomsen\ResumeGenerator\Reader\JSON\Reader
	 */
	public function personContactsIsRight(): void
	{
		$reader = new Reader(new SplFileInfo(__DIR__ . '/valid.json'));
		$resume = $reader->read();

		$expectedValues = [
			'Nowhere, to be found',
			'tom.tomsen@mailbox.org',
			'+00 000 0000',
			'/tomtomsen',
			'/tom-tomsen',
		];
		$contacts = $resume->person()->contacts();

		foreach ($contacts as $i => $contact) {
			self::assertSame($expectedValues[$i], $contact->contact());
		}
	}

	/**
	 * @test
	 *
	 * @covers ::parse
	 * @covers ::parseExperience
	 *
	 * @uses ::parsePerson
	 * @uses ::parseCertificate
	 * @uses ::parseDuration
	 * @uses ::parseEducation
	 * @covers ::read
	 *
	 * @uses ::parseInstitution
	 * @uses ::parseOrganisation
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Date
	 * @uses \tomtomsen\ResumeGenerator\Models\Duration
	 * @uses \tomtomsen\ResumeGenerator\Models\Education
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
	 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution
	 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
	 * @uses \tomtomsen\ResumeGenerator\Models\Location
	 * @uses \tomtomsen\ResumeGenerator\Models\Url
	 * @uses \tomtomsen\ResumeGenerator\Reader\JSON\Reader
	 */
	public function experienceTitleIsRight(): void
	{
		$reader = new Reader(new SplFileInfo(__DIR__ . '/valid.json'));
		$resume = $reader->read();

		$experiences = $resume->experiences();
		$experience = $experiences->get(0);

		self::assertSame('web-developer', (string) $experience->title());
	}
}
