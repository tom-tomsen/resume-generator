<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\Technology;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\Technology;
use function utf8_decode;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\Technology
 *
 * @internal
 *
 * @small
 */
final class NotEmptyTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function emptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Technology::fromString('');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function trimmedEmptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Technology::fromString('  ');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function trimmedEmptyUtf8String(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Technology::fromString((string) utf8_decode("\x20"));
	}
}
