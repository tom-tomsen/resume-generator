<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\Technology;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\Technology;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\Technology
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 */
	public function validDateFormat(): void
	{
		$name = 'PhP';
		$technology = Technology::fromString($name);

		self::assertSame($name, $technology->toString());
		self::assertSame($name, (string) $technology);
	}
}
