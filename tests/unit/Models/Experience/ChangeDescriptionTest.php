<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\RandomGenerator as RandomExperienceGenerator;
use function trim;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class ChangeDescriptionTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::description
	 */
	public function expecteDefaultDescriptionToBeEmpty(): void
	{
		$education = RandomExperienceGenerator::generate();

		self::assertSame('', $education->description());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::changeDescription
	 * @covers ::description
	 */
	public function changeDescription(): void
	{
		$education = RandomExperienceGenerator::generate();

		$description = 'my new description';
		$education->changeDescription($description);

		self::assertSame($description, $education->description());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::changeDescription
	 * @covers ::description
	 */
	public function descriptionGetsTrimmed(): void
	{
		$education = RandomExperienceGenerator::generate();

		$description = '     a description to be trimmed       ';
		$education->changeDescription($description);

		self::assertSame(trim($description), $education->description());
	}
}
