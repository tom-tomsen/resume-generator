<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\TechnologyCollection;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\Technology;
use tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
 *
 * @internal
 *
 * @small
 */
final class GetTechnologyTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function getExistingTechnology(): void
	{
		$collection = new TechnologyCollection();

		$technology = Technology::fromString('data');
		$collection->add($technology);

		self::assertNotEmpty($collection);
		self::assertSame($technology, $collection->get(0));
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function getNotExistingTechnology(): void
	{
		$collection = new TechnologyCollection();

		$technology = Technology::fromString('data');
		$collection->add($technology);

		$this->expectException(InvalidArgumentException::class);
		self::assertSame($technology, $collection->get(1));
	}
}
