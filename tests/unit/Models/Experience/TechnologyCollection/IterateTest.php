<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\TechnologyCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\Technology;
use tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection;
use Traversable;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
 *
 * @internal
 *
 * @small
 */
final class IterateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::getIterator
	 */
	public function iterateOverTechnologies(): void
	{
		$collection = new TechnologyCollection();

		$technology = Technology::fromString('data');
		$collection->add($technology);

		self::assertInstanceOf(Traversable::class, $collection);
		self::assertInstanceOf(Traversable::class, $collection->getIterator());
	}
}
