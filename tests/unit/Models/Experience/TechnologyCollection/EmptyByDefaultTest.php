<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\TechnologyCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 *
 * @internal
 *
 * @small
 */
final class EmptyByDefaultTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::count
	 */
	public function byDefaultItsEmpty(): void
	{
		$collection = new TechnologyCollection();

		self::assertEmpty($collection);
	}
}
