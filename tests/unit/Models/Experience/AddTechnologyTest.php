<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\Technology;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\RandomGenerator as RandomExperienceGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class AddTechnologyTest extends TestCase
{
	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::technologies
	 */
	public function initialTechnologyListIsEmpty(): void
	{
		$experience = RandomExperienceGenerator::generate();

		self::assertEmpty($experience->technologies());
	}

	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::technologies
	 */
	public function addTechnology(): void
	{
		$experience = RandomExperienceGenerator::generate();

		$technology = Technology::fromString('PHP');
		$experience->technologies()->add($technology);

		$technologies = $experience->technologies();
		self::assertNotEmpty($technologies);
		self::assertSame($technology, $technologies->get(0));
	}
}
