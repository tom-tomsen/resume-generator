<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience;

use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Experience;
use tomtomsen\ResumeGenerator\Models\Experience\Title;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration\RandomGenerator as RandomDurationGenerator;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Organisation\RandomGenerator as RandomOrganisationGenerator;

final class RandomGenerator
{
	public static function generate(): Experience
	{
		$faker = FakerFactory::create();

		$title = Title::fromString($faker->jobTitle);

		$duration = RandomDurationGenerator::generate();
		$organisation = RandomOrganisationGenerator::generate();

		return new Experience($title, $duration, $organisation);
	}
}
