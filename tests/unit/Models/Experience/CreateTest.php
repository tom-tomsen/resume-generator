<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;
use tomtomsen\ResumeGenerator\Models\Experience;
use tomtomsen\ResumeGenerator\Models\Experience\Title;
use tomtomsen\ResumeGenerator\Models\Location;
use tomtomsen\ResumeGenerator\Models\Organisation;
use tomtomsen\ResumeGenerator\Models\Organisation\Name;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Technology
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::title
	 */
	public function titleGetsSet(): void
	{
		$title = Title::fromString('Web-Developer');

		$start = Date::fromString('01-2000');
		$end = Date::fromString('01-2005');
		$duration = new Duration($start, $end);

		$name = Name::fromString('Sample Corp.');
		$location = Location::fromString('Vienna');
		$organisation = new Organisation($name, $location);

		$experience = new Experience($title, $duration, $organisation);

		self::assertSame($title, $experience->title());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::duration
	 */
	public function durationGetsSet(): void
	{
		$title = Title::fromString('Web-Developer');

		$start = Date::fromString('01-2000');
		$end = Date::fromString('01-2005');
		$duration = new Duration($start, $end);

		$name = Name::fromString('Sample Corp.');
		$location = Location::fromString('Vienna');
		$organisation = new Organisation($name, $location);

		$experience = new Experience($title, $duration, $organisation);

		self::assertSame($duration, $experience->duration());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::organisation
	 */
	public function organisationGetsSet(): void
	{
		$title = Title::fromString('Web-Developer');

		$start = Date::fromString('01-2000');
		$end = Date::fromString('01-2005');
		$duration = new Duration($start, $end);

		$name = Name::fromString('Sample Corp.');
		$location = Location::fromString('Vienna');
		$organisation = new Organisation($name, $location);

		$experience = new Experience($title, $duration, $organisation);

		self::assertSame($organisation, $experience->organisation());
	}
}
