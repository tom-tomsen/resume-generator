<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\ExperienceCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\RandomGenerator as RandomExperienceGenerator;
use Traversable;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Experience
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class IterateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::getIterator
	 */
	public function iterateOverExperiences(): void
	{
		$collection = new ExperienceCollection();

		$Experience = RandomExperienceGenerator::generate();
		$collection->add($Experience);

		self::assertInstanceOf(Traversable::class, $collection);
		self::assertInstanceOf(Traversable::class, $collection->getIterator());
	}
}
