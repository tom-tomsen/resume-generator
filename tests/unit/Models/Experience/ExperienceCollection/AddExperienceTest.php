<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\ExperienceCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\RandomGenerator as RandomExperienceGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Experience
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class AddExperienceTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function addExperience(): void
	{
		$collection = new ExperienceCollection();

		$Experience = RandomExperienceGenerator::generate();
		$collection->add($Experience);

		self::assertNotEmpty($collection);
		self::assertSame($Experience, $collection->get(0));
	}
}
