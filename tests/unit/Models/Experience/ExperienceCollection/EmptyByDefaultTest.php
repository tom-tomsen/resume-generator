<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\ExperienceCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 *
 * @internal
 *
 * @small
 */
final class EmptyByDefaultTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::count
	 */
	public function byDefaultItsEmpty(): void
	{
		$Experience = new ExperienceCollection();

		self::assertEmpty($Experience);
	}
}
