<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Organisation\Name;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Organisation\Name;
use function utf8_decode;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Organisation\Name
 *
 * @internal
 *
 * @small
 */
final class NotEmptyTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function emptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Name::fromString('');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function trimmedEmptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Name::fromString('  ');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function trimmedEmptyUtf8String(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Name::fromString((string) utf8_decode("\x20"));
	}
}
