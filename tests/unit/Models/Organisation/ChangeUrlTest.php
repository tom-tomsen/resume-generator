<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Organisation;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Url;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Organisation\RandomGenerator as RandomOrganisationGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Organisation
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Url
 *
 * @internal
 *
 * @small
 */
final class ChangeUrlTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::url
	 */
	public function expecteDefaultUrlToBeEmpty(): void
	{
		$organisation = RandomOrganisationGenerator::generate();

		$url = $organisation->url();

		self::assertNull($url);
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::assignUrl
	 * @covers ::url
	 */
	public function assignUrl(): void
	{
		$organisation = RandomOrganisationGenerator::generate();

		$url = Url::fromString('http://www.google.com');
		$organisation->assignUrl($url);

		self::assertSame($url, $organisation->url());
	}
}
