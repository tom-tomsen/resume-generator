<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Organisation;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Location;
use tomtomsen\ResumeGenerator\Models\Organisation;
use tomtomsen\ResumeGenerator\Models\Organisation\Name;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Organisation
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::name
	 */
	public function nameGetsSet(): void
	{
		$name = Name::fromString('My Corporation');
		$location = Location::fromString('Vienna');

		$organisation = new Organisation($name, $location);

		self::assertSame($name, $organisation->name());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::location
	 */
	public function locationGetsSet(): void
	{
		$name = Name::fromString('My Corporation');
		$location = Location::fromString('Vienna');

		$organisation = new Organisation($name, $location);

		self::assertSame($location, $organisation->location());
	}
}
