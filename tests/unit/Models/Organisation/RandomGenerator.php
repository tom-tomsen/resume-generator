<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Organisation;

use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Location;
use tomtomsen\ResumeGenerator\Models\Organisation;
use tomtomsen\ResumeGenerator\Models\Organisation\Name;

final class RandomGenerator
{
	public static function generate(): Organisation
	{
		$faker = FakerFactory::create();

		return new Organisation(
			Name::fromString($faker->name),
			Location::fromString($faker->country)
		);
	}
}
