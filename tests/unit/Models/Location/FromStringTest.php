<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Location;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Location;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 */
	public function validDateFormat(): void
	{
		$str = 'abcäöp';
		$title = Location::fromString($str);

		self::assertSame($str, $title->toString());
		self::assertSame($str, (string) $title);
	}
}
