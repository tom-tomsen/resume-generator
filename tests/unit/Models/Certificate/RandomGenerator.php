<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate;

use DateTimeImmutable;
use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Certificate;
use tomtomsen\ResumeGenerator\Models\Certificate\Title;
use tomtomsen\ResumeGenerator\Models\Date;

final class RandomGenerator
{
	public static function generate(): Certificate
	{
		$faker = FakerFactory::create();

		$title = Title::fromString($faker->jobTitle);
		$date = new Date(
			DateTimeImmutable::createFromMutable(
				$faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now')
			)
		);

		return new Certificate($title, $date);
	}
}
