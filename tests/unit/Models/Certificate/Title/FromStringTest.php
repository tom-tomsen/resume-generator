<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\Title;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Certificate\Title;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\Title
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 */
	public function validDateFormat(): void
	{
		$str = 'abcäöp';
		$title = Title::fromString($str);

		self::assertSame($str, $title->toString());
		self::assertSame($str, (string) $title);
	}
}
