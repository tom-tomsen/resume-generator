<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\CertificateCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 *
 * @internal
 *
 * @small
 */
final class EmptyByDefaultTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::count
	 */
	public function byDefaultItsEmpty(): void
	{
		$certificate = new CertificateCollection();

		self::assertEmpty($certificate);
	}
}
