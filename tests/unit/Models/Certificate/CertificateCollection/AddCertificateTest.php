<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\CertificateCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\RandomGenerator as RandomCertificateGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @uses \tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\RandomGenerator
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
 *
 * @internal
 *
 * @small
 */
final class AddCertificateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function addCertificate(): void
	{
		$collection = new CertificateCollection();

		$certificate = RandomCertificateGenerator::generate();
		$collection->add($certificate);

		self::assertNotEmpty($collection);
		self::assertSame($certificate, $collection->get(0));
	}
}
