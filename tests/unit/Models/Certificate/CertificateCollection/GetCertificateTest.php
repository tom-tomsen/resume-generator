<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\CertificateCollection;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\RandomGenerator as RandomCertificateGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
 *
 * @internal
 *
 * @small
 */
final class GetCertificateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function getExistingCertificate(): void
	{
		$collection = new CertificateCollection();

		$Certificate = RandomCertificateGenerator::generate();
		$collection->add($Certificate);

		self::assertNotEmpty($collection);
		self::assertSame($Certificate, $collection->get(0));
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::get
	 */
	public function getNotExistingCertificate(): void
	{
		$collection = new CertificateCollection();

		$Certificate = RandomCertificateGenerator::generate();
		$collection->add($Certificate);

		$this->expectException(InvalidArgumentException::class);
		self::assertSame($Certificate, $collection->get(1));
	}
}
