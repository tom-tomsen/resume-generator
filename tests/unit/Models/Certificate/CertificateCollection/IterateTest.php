<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\CertificateCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\RandomGenerator as RandomCertificateGenerator;
use Traversable;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
 *
 * @internal
 *
 * @small
 */
final class IterateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::getIterator
	 */
	public function iterateOverCertificates(): void
	{
		$collection = new CertificateCollection();

		$Certificate = RandomCertificateGenerator::generate();
		$collection->add($Certificate);

		self::assertInstanceOf(Traversable::class, $collection);
		self::assertInstanceOf(Traversable::class, $collection->getIterator());
	}
}
