<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Certificate;
use tomtomsen\ResumeGenerator\Models\Certificate\Title;
use tomtomsen\ResumeGenerator\Models\Date;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Certificate
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::date
	 */
	public function dateGetsSet(): void
	{
		$title = Title::fromString('some title');
		$date = Date::fromString('01-2000');

		$certificate = new Certificate($title, $date);

		self::assertSame($date, $certificate->date());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::title
	 */
	public function titleGetsSet(): void
	{
		$title = Title::fromString('some title');
		$date = Date::fromString('01-2000');

		$certificate = new Certificate($title, $date);

		self::assertSame($title, $certificate->title());
	}
}
