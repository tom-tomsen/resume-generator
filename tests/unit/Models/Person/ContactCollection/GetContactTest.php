<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\ContactCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Contact;
use tomtomsen\ResumeGenerator\Models\Person\ContactCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Contact
 *
 * @internal
 *
 * @small
 */
final class GetContactTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function getExistingType(): void
	{
		$collection = new ContactCollection();

		$contact = new Contact('type-1', 'data');
		$collection->add($contact);

		self::assertNotEmpty($collection);
		self::assertSame([$contact], $collection->get('type-1'));
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function getNotExistingType(): void
	{
		$collection = new ContactCollection();

		$contact = new Contact('type-1', 'data');
		$collection->add($contact);

		self::assertSame([], $collection->get('type-2'));
	}
}
