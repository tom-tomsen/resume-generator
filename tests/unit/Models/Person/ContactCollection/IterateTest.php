<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\ContactCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Contact;
use tomtomsen\ResumeGenerator\Models\Person\ContactCollection;
use Traversable;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Contact
 *
 * @internal
 *
 * @small
 */
final class IterateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::getIterator
	 */
	public function iterateOverContacts(): void
	{
		$collection = new ContactCollection();

		$contact = new Contact('type-1', 'data');
		$collection->add($contact);

		self::assertInstanceOf(Traversable::class, $collection);
		self::assertInstanceOf(Traversable::class, $collection->getIterator());
	}
}
