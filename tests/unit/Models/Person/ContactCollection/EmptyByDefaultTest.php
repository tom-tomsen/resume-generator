<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\ContactCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\ContactCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @internal
 *
 * @small
 */
final class EmptyByDefaultTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::count
	 */
	public function byDefaultItsEmpty(): void
	{
		$contact = new ContactCollection();

		self::assertEmpty($contact);
	}
}
