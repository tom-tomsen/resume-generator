<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Contact;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\RandomGenerator as RandomPersonGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Contact
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @internal
 *
 * @small
 */
final class AddContactTest extends TestCase
{
	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::contacts
	 */
	public function byDefaultContactsAreEmpty(): void
	{
		$person = RandomPersonGenerator::generate();

		self::assertEmpty($person->contacts());
	}

	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::contacts
	 */
	public function addContact(): void
	{
		$person = RandomPersonGenerator::generate();

		$contact = new Contact('type-1', 'my contact');
		$person->contacts()->add($contact);

		$contacts = $person->contacts();
		self::assertCount(1, $contacts);
		self::assertTrue($contacts->has('type-1'));
		self::assertSame($contact, $contacts->get('type-1')[0]);
	}

	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::contacts
	 */
	public function addDuplicateContactIsOk(): void
	{
		$person = RandomPersonGenerator::generate();

		$contact = new Contact('type-1', 'my contact');
		$person->contacts()->add($contact);
		$person->contacts()->add($contact);

		$contacts = $person->contacts();
		self::assertCount(2, $contacts);
		self::assertSame($contact, $contacts->get('type-1')[0]);
		self::assertSame($contact, $contacts->get('type-1')[1]);
	}
}
