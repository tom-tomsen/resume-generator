<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Image;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\RandomGenerator as RandomPersonGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Image
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @internal
 *
 * @small
 */
final class ChangeImageTest extends TestCase
{
	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::image
	 */
	public function byDefaultNoImage(): void
	{
		$person = RandomPersonGenerator::generate();

		self::assertNull($person->image());
	}

	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::changeImage
	 * @covers ::image
	 */
	public function changeImage(): void
	{
		$person = RandomPersonGenerator::generate();

		$image = Image::fromPath(__DIR__ . '/../../fixtures/image.png');
		$person->changeImage($image);

		self::assertSame($image, $person->image());
	}
}
