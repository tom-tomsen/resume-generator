<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person;

use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Person;
use tomtomsen\ResumeGenerator\Models\Person\Name;
use tomtomsen\ResumeGenerator\Models\Person\Title;

final class RandomGenerator
{
	public static function generate(): Person
	{
		$faker = FakerFactory::create();

		return new Person(
			Name::fromString($faker->name()),
			Title::fromString($faker->jobTitle)
		);
	}
}
