<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\Contact;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Contact;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\Contact
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person\Contact
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::__toString
	 * @covers ::contact
	 * @covers ::toString
	 */
	public function asString(): void
	{
		$str = 'contact 1234';
		$contact = new Contact('type-1', $str);

		self::assertSame($str, $contact->toString());
		self::assertSame($str, (string) $contact);
		self::assertSame($str, $contact->contact());
	}
}
