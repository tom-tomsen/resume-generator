<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\Contact;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Contact;
use function utf8_decode;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\Contact
 *
 * @internal
 *
 * @small
 */
final class ContactNotEmptyTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 */
	public function emptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		new Contact('type-1', '');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 */
	public function trimmedEmptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		new Contact('type-1', '  ');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 */
	public function trimmedEmptyUtf8String(): void
	{
		$this->expectException(InvalidArgumentException::class);
		new Contact('type-1', (string) utf8_decode("\x20"));
	}
}
