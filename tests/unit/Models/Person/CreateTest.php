<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person;
use tomtomsen\ResumeGenerator\Models\Person\Name;
use tomtomsen\ResumeGenerator\Models\Person\Title;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::name
	 */
	public function nameGetsSet(): void
	{
		$name = Name::fromString('My Corporation');
		$title = Title::fromString('Vienna');

		$person = new Person($name, $title);

		self::assertSame($name, $person->name());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::title
	 */
	public function titleGetsSet(): void
	{
		$name = Name::fromString('My Corporation');
		$title = Title::fromString('Vienna');

		$person = new Person($name, $title);

		self::assertSame($title, $person->title());
	}
}
