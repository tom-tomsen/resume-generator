<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\Title;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Title;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\Title
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 */
	public function validDateFormat(): void
	{
		$str = 'abcäöp';
		$title = Title::fromString($str);

		self::assertSame($str, $title->toString());
		self::assertSame($str, (string) $title);
	}
}
