<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\Name;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person\Name;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person\Name
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 */
	public function validDateFormat(): void
	{
		$str = 'abcäöp';
		$name = Name::fromString($str);

		self::assertSame($str, $name->toString());
		self::assertSame($str, (string) $name);
	}
}
