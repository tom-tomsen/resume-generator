<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Person;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\RandomGenerator as RandomPersonGenerator;
use function trim;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Person
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 *
 * @internal
 *
 * @small
 */
final class ChangeDescriptionTest extends TestCase
{
	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::description
	 */
	public function byDefaultDescriptionIsEmpty(): void
	{
		$person = RandomPersonGenerator::generate();

		self::assertSame('', $person->description());
	}

	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::changeDescription
	 * @covers ::description
	 */
	public function changeDescription(): void
	{
		$person = RandomPersonGenerator::generate();

		$description = 'my description';
		$person->changeDescription($description);

		self::assertSame($description, $person->description());
	}

	/**
	 * @test
	 *
	 * @uses ::__construct
	 * @covers ::changeDescription
	 * @covers ::description
	 */
	public function descriptionGetsTrimmed(): void
	{
		$person = RandomPersonGenerator::generate();

		$description = '     my description    ';
		$person->changeDescription($description);

		self::assertSame(trim($description), $person->description());
	}
}
