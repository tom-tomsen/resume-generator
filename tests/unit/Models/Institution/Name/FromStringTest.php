<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Institution\Name;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Institution\Name;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Institution\Name
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 */
	public function validDateFormat(): void
	{
		$str = 'abcäöp';
		$title = Name::fromString($str);

		self::assertSame($str, $title->toString());
		self::assertSame($str, (string) $title);
	}
}
