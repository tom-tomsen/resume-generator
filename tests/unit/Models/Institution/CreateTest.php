<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Institution;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Institution;
use tomtomsen\ResumeGenerator\Models\Institution\Name;
use tomtomsen\ResumeGenerator\Models\Location;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Institution
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::name
	 */
	public function nameGetsSet(): void
	{
		$name = Name::fromString('University');
		$location = Location::fromString('Vienna');

		$institution = new Institution($name, $location);

		self::assertSame($name, $institution->name());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::location
	 */
	public function locationGetsSet(): void
	{
		$name = Name::fromString('University');
		$location = Location::fromString('Vienna');

		$institution = new Institution($name, $location);

		self::assertSame($location, $institution->location());
	}
}
