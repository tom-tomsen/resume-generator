<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Institution;

use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Institution;
use tomtomsen\ResumeGenerator\Models\Institution\Name;
use tomtomsen\ResumeGenerator\Models\Location;

final class RandomGenerator
{
	public static function generate(): Institution
	{
		$faker = FakerFactory::create();

		$name = Name::fromString($faker->company);
		$location = Location::fromString($faker->city);

		return new Institution($name, $location);
	}
}
