<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\EducationCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Education\EducationCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\RandomGenerator as RandomEducationGenerator;
use Traversable;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Education
 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Institution
 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class IterateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::getIterator
	 */
	public function iterateOverEducations(): void
	{
		$collection = new EducationCollection();

		$education = RandomEducationGenerator::generate();
		$collection->add($education);

		self::assertInstanceOf(Traversable::class, $collection);
		self::assertInstanceOf(Traversable::class, $collection->getIterator());
	}
}
