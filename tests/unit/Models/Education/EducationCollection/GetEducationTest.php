<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\EducationCollection;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Education\EducationCollection;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\RandomGenerator as RandomEducationGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Education
 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Institution
 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class GetEducationTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::count
	 * @covers ::get
	 */
	public function getExistingEducation(): void
	{
		$collection = new EducationCollection();

		$education = RandomEducationGenerator::generate();
		$collection->add($education);

		self::assertNotEmpty($collection);
		self::assertSame($education, $collection->get(0));
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::add
	 * @covers ::get
	 */
	public function getNotExistingEducation(): void
	{
		$collection = new EducationCollection();

		$education = RandomEducationGenerator::generate();
		$collection->add($education);

		$this->expectException(InvalidArgumentException::class);
		self::assertSame($education, $collection->get(1));
	}
}
