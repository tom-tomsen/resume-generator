<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\EducationCollection;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Education\EducationCollection;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 *
 * @internal
 *
 * @small
 */
final class EmptyByDefaultTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::count
	 */
	public function byDefaultItsEmpty(): void
	{
		$education = new EducationCollection();

		self::assertEmpty($education);
	}
}
