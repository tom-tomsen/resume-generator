<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\Title;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Education\Title;
use function utf8_decode;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Education\Title
 *
 * @internal
 *
 * @small
 */
final class NotEmptyTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function emptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Title::fromString('');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function trimmedEmptyString(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Title::fromString('  ');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 */
	public function trimmedEmptyUtf8String(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Title::fromString((string) utf8_decode("\x20"));
	}
}
