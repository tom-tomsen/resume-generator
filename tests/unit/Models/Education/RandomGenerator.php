<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education;

use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Education;
use tomtomsen\ResumeGenerator\Models\Education\Title;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration\RandomGenerator as RandomDurationGenerator;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Institution\RandomGenerator as RandomInstitutionGenerator;

final class RandomGenerator
{
	public static function generate(): Education
	{
		$faker = FakerFactory::create();

		$institution = RandomInstitutionGenerator::generate();
		$duration = RandomDurationGenerator::generate();

		$title = Title::fromString($faker->jobTitle);

		return new Education($title, $duration, $institution);
	}
}
