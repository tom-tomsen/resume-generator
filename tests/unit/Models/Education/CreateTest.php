<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;
use tomtomsen\ResumeGenerator\Models\Education;
use tomtomsen\ResumeGenerator\Models\Education\Title;
use tomtomsen\ResumeGenerator\Models\Institution;
use tomtomsen\ResumeGenerator\Models\Institution\Name;
use tomtomsen\ResumeGenerator\Models\Location;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Education
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Institution
 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::title
	 */
	public function titleGetsSet(): void
	{
		$name = Name::fromString('University');
		$location = Location::fromString('Vienna');
		$institution = new Institution($name, $location);

		$title = Title::fromString('Web-Developer');
		$start = Date::fromString('01-2000');
		$end = Date::fromString('01-2005');
		$duration = new Duration($start, $end);

		$education = new Education($title, $duration, $institution);

		self::assertSame($title, $education->title());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::duration
	 */
	public function durationGetsSet(): void
	{
		$name = Name::fromString('University');
		$location = Location::fromString('Vienna');
		$institution = new Institution($name, $location);

		$title = Title::fromString('Web-Developer');
		$start = Date::fromString('01-2000');
		$end = Date::fromString('01-2005');
		$duration = new Duration($start, $end);

		$education = new Education($title, $duration, $institution);

		self::assertSame($duration, $education->duration());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::institution
	 */
	public function institutionGetsSet(): void
	{
		$name = Name::fromString('University');
		$location = Location::fromString('Vienna');
		$institution = new Institution($name, $location);

		$title = Title::fromString('Web-Developer');
		$start = Date::fromString('01-2000');
		$end = Date::fromString('01-2005');
		$duration = new Duration($start, $end);

		$education = new Education($title, $duration, $institution);

		self::assertSame($institution, $education->institution());
	}
}
