<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Education;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\RandomGenerator as RandomEducationGenerator;
use function trim;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Education
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Institution
 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class ChangeDescriptionTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::description
	 */
	public function expecteDefaultDescriptionToBeEmpty(): void
	{
		$education = RandomEducationGenerator::generate();

		self::assertSame('', $education->description());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::changeDescription
	 * @covers ::description
	 */
	public function changeDescription(): void
	{
		$education = RandomEducationGenerator::generate();

		$description = 'my new description';
		$education->changeDescription($description);

		self::assertSame($description, $education->description());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::changeDescription
	 * @covers ::description
	 */
	public function descriptionGetsTrimmed(): void
	{
		$education = RandomEducationGenerator::generate();

		$description = '     a description to be trimmed       ';
		$education->changeDescription($description);

		self::assertSame(trim($description), $education->description());
	}
}
