<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Url;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Url;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Url
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Url
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 *
	 * @uses ::allowedUrlSchemes
	 */
	public function invalidScheme(): void
	{
		self::expectException(InvalidArgumentException::class);
		Url::fromString('ftp://www.example.org');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::fromString
	 *
	 * @uses ::allowedUrlSchemes
	 */
	public function noScheme(): void
	{
		self::expectException(InvalidArgumentException::class);
		Url::fromString('xxx');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::__toString
	 * @covers ::fromString
	 * @covers ::toString
	 *
	 * @uses ::allowedUrlSchemes
	 *
	 * @testWith ["http://example.org", "https://example.org", "mailto:mail@example.org"]
	 */
	public function validUrlFormat(string $urlStr): void
	{
		$url = Url::fromString($urlStr);

		self::assertSame($urlStr, $url->toString());
		self::assertSame($urlStr, (string) $url);
	}

	/**
	 * @test
	 *
	 * @covers ::allowedUrlSchemes
	 */
	public function allowedUrlSchemesNotEmpty(): void
	{
		$allowedSchemes = Url::allowedUrlSchemes();

		self::assertNotEmpty($allowedSchemes);
	}
}
