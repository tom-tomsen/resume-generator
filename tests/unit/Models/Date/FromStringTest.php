<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Date;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class FromStringTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::fromString
	 */
	public function invalidDateFormat(): void
	{
		$this->expectException(InvalidArgumentException::class);
		Date::fromString('01-2000', 'd');
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::format
	 * @covers ::fromString
	 */
	public function validDateFormat(): void
	{
		$date = Date::fromString('01-2000', 'd-Y');

		self::assertSame('01-2000', $date->format('d-Y'));
	}
}
