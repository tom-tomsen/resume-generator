<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Date;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::date
	 */
	public function dateGetsSet(): void
	{
		$now = new DateTimeImmutable();
		$date = new Date($now);

		self::assertSame($now, $date->date());
	}
}
