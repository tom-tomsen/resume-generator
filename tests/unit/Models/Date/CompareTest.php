<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Date;

use DateInterval;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class CompareTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::equals
	 */
	public function equals(): void
	{
		$now = new DateTimeImmutable();
		$before = (clone $now)->sub(new DateInterval('PT30S'));
		$after = (clone $now)->add(new DateInterval('PT30S'));

		$date = new Date($now);
		$beforeDate = new Date($before);
		$afterDate = new Date($after);

		self::assertSame(0, $date->equals($date));
		self::assertSame(+1, $date->equals($beforeDate));
		self::assertSame(-1, $date->equals($afterDate));
	}
}
