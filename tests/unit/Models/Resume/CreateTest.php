<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Person;
use tomtomsen\ResumeGenerator\Models\Resume;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\RandomGenerator as RandomPersonGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Person
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::person
	 */
	public function personGetsSet(): void
	{
		$person = RandomPersonGenerator::generate();

		$resume = new Resume($person);

		self::assertSame($person, $resume->person());
	}
}
