<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Experience\RandomGenerator as RandomExperienceGenerator;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume\RandomGenerator as RandomResumeGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Experience
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation
 * @uses \tomtomsen\ResumeGenerator\Models\Organisation\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class AddExperienceTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::experiences
	 */
	public function byDefaultEmpty(): void
	{
		$resume = RandomResumeGenerator::generate();

		self::assertEmpty($resume->experiences());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::experiences
	 */
	public function addExperience(): void
	{
		$resume = RandomResumeGenerator::generate();
		$experience = RandomExperienceGenerator::generate();

		$resume->experiences()->add($experience);

		$actualExperienceList = $resume->experiences();
		self::assertNotEmpty($actualExperienceList);
		self::assertSame($experience, $actualExperienceList->get(0));
	}
}
