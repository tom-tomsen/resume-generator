<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Certificate\RandomGenerator as RandomCertificateGenerator;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume\RandomGenerator as RandomResumeGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Person
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class AddCertificateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::certificates
	 */
	public function byDefaultEmpty(): void
	{
		$resume = RandomResumeGenerator::generate();

		self::assertEmpty($resume->certificates());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::certificates
	 */
	public function addCertificate(): void
	{
		$resume = RandomResumeGenerator::generate();
		$certificate = RandomCertificateGenerator::generate();

		$resume->certificates()->add($certificate);

		$actualCertificateList = $resume->certificates();
		self::assertNotEmpty($actualCertificateList);
		self::assertSame($certificate, $actualCertificateList->get(0));
	}
}
