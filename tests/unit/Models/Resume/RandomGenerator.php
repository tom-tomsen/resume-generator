<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume;

use tomtomsen\ResumeGenerator\Models\Resume;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Person\RandomGenerator as RandomPersonGenerator;

final class RandomGenerator
{
	public static function generate(): Resume
	{
		$person = RandomPersonGenerator::generate();

		return new Resume($person);
	}
}
