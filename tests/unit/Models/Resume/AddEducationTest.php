<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Education\RandomGenerator as RandomEducationGenerator;
use tomtomsen\ResumeGenerator\Tests\Unit\Models\Resume\RandomGenerator as RandomResumeGenerator;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 * @usesDefaultClass \tomtomsen\ResumeGenerator\Models\Resume
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Person
 * @uses \tomtomsen\ResumeGenerator\Models\Person\ContactCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Person\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 * @uses \tomtomsen\ResumeGenerator\Models\Duration
 * @uses \tomtomsen\ResumeGenerator\Models\Education
 * @uses \tomtomsen\ResumeGenerator\Models\Education\Title
 * @uses \tomtomsen\ResumeGenerator\Models\Education\EducationCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection
 * @uses \tomtomsen\ResumeGenerator\Models\Institution
 * @uses \tomtomsen\ResumeGenerator\Models\Institution\Name
 * @uses \tomtomsen\ResumeGenerator\Models\Location
 *
 * @internal
 *
 * @small
 */
final class AddEducationTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::educations
	 */
	public function byDefaultEmpty(): void
	{
		$resume = RandomResumeGenerator::generate();

		self::assertEmpty($resume->educations());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::educations
	 */
	public function addEducation(): void
	{
		$resume = RandomResumeGenerator::generate();
		$education = RandomEducationGenerator::generate();

		$resume->educations()->add($education);

		$actualEducationList = $resume->educations();
		self::assertNotEmpty($actualEducationList);
		self::assertSame($education, $actualEducationList->get(0));
	}
}
