<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Duration
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class SwappedEndsAreInvalidTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 */
	public function endBeforeStart(): void
	{
		$start = Date::fromString('05-2000');
		$end = Date::fromString('04-2000');

		$this->expectException(InvalidArgumentException::class);
		$duration = new Duration($start, $end);
	}
}
