<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration;

use DateTimeImmutable;
use Faker\Factory as FakerFactory;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;

final class RandomGenerator
{
	public static function generate(): Duration
	{
		$faker = FakerFactory::create();

		$start = new Date(
			DateTimeImmutable::createFromMutable(
				$faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now')
			)
		);
		$end = new Date(
			DateTimeImmutable::createFromMutable(
				$faker->dateTimeBetween($startDate = 'now', $endDate = '+30 years')
			)
		);

		return new Duration($start, $end);
	}
}
