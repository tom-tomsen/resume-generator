<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Duration
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class CreateTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::start
	 */
	public function startGetsSet(): void
	{
		$start = Date::fromString('03-2000');
		$end = Date::fromString('04-2000');

		$duration = new Duration($start, $end);

		self::assertSame($start, $duration->start());
	}

	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::end
	 */
	public function endGetsSet(): void
	{
		$start = Date::fromString('03-2000');
		$end = Date::fromString('04-2000');

		$duration = new Duration($start, $end);

		self::assertSame($end, $duration->end());
	}
}
