<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Duration
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class WithOpenEndTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::end
	 * @covers ::hasEnd
	 */
	public function hasNoEnd(): void
	{
		$start = Date::fromString('01-2000');

		$duration = new Duration($start);

		self::assertFalse($duration->hasEnd());
		self::assertNull($duration->end());
	}
}
