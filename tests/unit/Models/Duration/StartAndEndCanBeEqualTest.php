<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Tests\Unit\Models\Duration;

use PHPUnit\Framework\TestCase;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;

/**
 * @coversDefaultClass \tomtomsen\ResumeGenerator\Models\Duration
 *
 * @uses \tomtomsen\ResumeGenerator\Models\Date
 *
 * @internal
 *
 * @small
 */
final class StartAndEndCanBeEqualTest extends TestCase
{
	/**
	 * @test
	 *
	 * @covers ::__construct
	 * @covers ::end
	 * @covers ::start
	 */
	public function startEqualsEnd(): void
	{
		$start = Date::fromString('03-2000');
		$end = Date::fromString('03-2000');

		$duration = new Duration($start, $end);

		self::assertSame($start, $duration->start());
		self::assertSame($end, $duration->end());
	}
}
