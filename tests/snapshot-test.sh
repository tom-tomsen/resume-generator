#!/usr/bin/env sh
set -e

cd "$(dirname $0)"/..

[ -d "tests/tmp" ] && rm -rf tests/tmp/*
[ ! -d "tests/tmp" ] && mkdir -p tests/tmp

./bin/generate
magick convert -background white -alpha remove -alpha off "build/resume.pdf" "tests/tmp/preview.png"

errors=0
tests=0
for file in ./tests/tmp/*.png; do
	base=$(basename $file)
	OUT=$(magick compare -metric AE "./build/${base}" "$file" "tests/tmp/diff-$base" 2>&1 || true)
	tests=$((tests + 1))
	if [ "0" != "$OUT" ]; then
		echo "Resume has changed (see tests/tmp/diff-$base)"
		errors=$((errors + 1))
	else
		rm $file
		rm "tests/tmp/diff-$base"
	fi
done

if [ 0 -eq $errors ]; then
	echo "OK - $tests assertions"
else
	echo "FAILED - $errors of $tests tests failed"
	exit 1
fi
