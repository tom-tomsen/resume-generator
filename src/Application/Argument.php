<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Application;

abstract class Argument
{
	/**
	 * @var string
	 */
	private $value;

	protected function __construct(string $value)
	{
		$this->value = $value;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	final public function toString(): string
	{
		return $this->value;
	}
}
