<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Application;

use Symfony\Component\Console\Application;

final class Console
{
	public static function run(): void
	{
		$application = new Application('generate-resume', '1.0.0');
		$command = new GenerateCommand();

		$application->add($command);

		$application->setDefaultCommand(GenerateCommand::NAME, TRUE);
		$application->run();
	}
}
