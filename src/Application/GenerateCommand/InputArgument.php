<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Application\GenerateCommand;

use RuntimeException;
use Symfony\Component\Console\Input\InputArgument as SymfonyInputArgument;
use Symfony\Component\Console\Input\InputInterface;
use tomtomsen\ResumeGenerator\Application\Argument;
use tomtomsen\ResumeGenerator\Application\GenerateCommand;
use function file_exists;
use function is_readable;
use function is_string;

final class InputArgument extends Argument
{
	public const NAME = 'input';

	protected function __construct(string $value)
	{
		if (!file_exists($value)) {
			throw new RuntimeException("missing file '{$value}'");
		}

		if (!is_readable($value)) {
			throw new RuntimeException("not readable '{$value}'");
		}

		parent::__construct($value);
	}

	public static function fromInput(InputInterface $input): Argument
	{
		$value = $input->getArgument(self::NAME);

		if (!is_string($value)) {
			throw new RuntimeException('invalid argument "out"');
		}

		return new self($value);
	}

	public static function addToCommand(GenerateCommand &$command): void
	{
		$command->addArgument(self::NAME, SymfonyInputArgument::REQUIRED, 'resume data file');
	}
}
