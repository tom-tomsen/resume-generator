<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Application\GenerateCommand;

use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption as SymfonyInputOption;
use tomtomsen\ResumeGenerator\Application\GenerateCommand;
use tomtomsen\ResumeGenerator\Application\Option;
use function is_string;

final class LocaleOption extends Option
{
	public const LONG_NAME = 'locale';

	public const SHORT_NAME = 'l';

	public const DEFAULT = 'de';

	public static function fromInput(InputInterface $input): Option
	{
		$value = $input->getOption(self::LONG_NAME);

		if (!is_string($value)) {
			throw new RuntimeException('invalid argument "out"');
		}

		return new self($value);
	}

	public static function addToCommand(GenerateCommand &$command): void
	{
		$command->addOption(self::LONG_NAME, self::SHORT_NAME, SymfonyInputOption::VALUE_OPTIONAL, 'locale', self::DEFAULT);
	}
}
