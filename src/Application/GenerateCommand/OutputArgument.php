<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Application\GenerateCommand;

use RuntimeException;
use Symfony\Component\Console\Input\InputArgument as SymfonyInputArgument;
use Symfony\Component\Console\Input\InputInterface;
use tomtomsen\ResumeGenerator\Application\Argument;
use tomtomsen\ResumeGenerator\Application\GenerateCommand;
use function dirname;
use function is_string;
use function is_writable;

final class OutputArgument extends Argument
{
	public const NAME = 'output';

	protected function __construct(string $value)
	{
		$directory = dirname($value);

		if (!is_writable($directory)) {
			throw new RuntimeException("directory not writeable '{$directory}'");
		}

		parent::__construct($value);
	}

	public static function fromInput(InputInterface $input): Argument
	{
		$value = $input->getArgument(self::NAME);

		if (!is_string($value)) {
			throw new RuntimeException('invalid argument "out"');
		}

		return new self($value);
	}

	public static function addToCommand(GenerateCommand &$command): void
	{
		$command->addArgument(self::NAME, SymfonyInputArgument::REQUIRED, 'output file name');
	}
}
