<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Application;

use Exception;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use tomtomsen\ResumeGenerator\Application\GenerateCommand\InputArgument;
use tomtomsen\ResumeGenerator\Application\GenerateCommand\LocaleOption;
use tomtomsen\ResumeGenerator\Application\GenerateCommand\OutputArgument;
use tomtomsen\ResumeGenerator\Converter\Factory as ConverterFactory;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Formatter\DurationFormatter;
use tomtomsen\ResumeGenerator\Formatter\TextFormatter;
use tomtomsen\ResumeGenerator\Reader\Factory as ReaderFactory;
use tomtomsen\ResumeGenerator\Translator\ArrayTranslator;
use tomtomsen\ResumeGenerator\Translator\PHPLocale;
use function file_put_contents;

final class GenerateCommand extends Command
{
	public const NAME = 'generate';

	protected static $defaultName = 'generate';

	protected function configure(): void
	{
		LocaleOption::addToCommand($this);
		InputArgument::addToCommand($this);
		OutputArgument::addToCommand($this);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$localeOption = LocaleOption::fromInput($input);
		$outputArg = OutputArgument::fromInput($input);
		$inputArg = InputArgument::fromInput($input);

		$locale = new PHPLocale("resources/locales/{$localeOption}.php");
		$translator = new ArrayTranslator($locale->i18n());

		$dateFormatter = new DateFormatter($locale->dateFormat(), $translator);
		$durationFormatter = new DurationFormatter($dateFormatter, $translator);
		$textFormatter = new TextFormatter();

		$jsonReader = ReaderFactory::get(new SplFileInfo((string) $inputArg));

		$resume = $jsonReader->read();

		$converter = ConverterFactory::get(
			new SplFileInfo((string) $outputArg),
			$resume,
			$translator,
			$dateFormatter,
			$durationFormatter,
			$textFormatter
		);

		try {
			$bytesWritten = file_put_contents((string) $outputArg, $converter->toString());

			if (0 < $bytesWritten) {
				$output->writeln('<info>' . (string) $outputArg . ' written</info>');
			} else {
				$output->writeln('<error>Cant generate resume. no output</error>');
			}
		} catch (Exception $ex) {
			$output->writeln('<error>Cant generate resume</error>');
		}

		return 0;
	}
}
