<?php

\date_default_timezone_set('UTC');
\setlocale(\LC_ALL, 'en_US');

require __DIR__ . '/../vendor/autoload.php';
