<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Translator;

use function strtr;

final class NoTranslator implements Translator
{
	/**
	 * @param array<string, string> $params
	 */
	public function translate(string $text, array $params = []): string
	{
		$p = [];

		foreach ($params as $name => $value) {
			$p['{' . $name . '}'] = $value;
		}

		return strtr($text, $p);
	}
}
