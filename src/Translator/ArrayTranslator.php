<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Translator;

use function array_key_exists;
use function strtr;

final class ArrayTranslator implements Translator
{
	/**
	 * @var array<string, string>
	 */
	private $translations;

	/**
	 * @param array<string, string> $translations
	 */
	public function __construct(array $translations)
	{
		$this->translations = $translations;
	}

	/**
	 * @param array<string, string> $params
	 */
	public function translate(string $text, array $params = []): string
	{
		$translation = $text;

		if (array_key_exists($text, $this->translations)) {
			$translation = $this->translations[$text];
		}

		$p = [];

		foreach ($params as $name => $value) {
			$p['{' . $name . '}'] = $value;
		}

		return strtr($translation, $p);
	}
}
