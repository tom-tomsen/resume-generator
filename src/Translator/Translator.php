<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Translator;

interface Translator
{
	/**
	 * @param array<string, string> $params
	 */
	public function translate(string $text, array $params = []): string;
}
