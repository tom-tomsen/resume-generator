<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Translator;

use RuntimeException;
use function array_key_exists;
use function ob_end_clean;
use function ob_start;

final class PHPLocale implements Locale
{
	/**
	 * @var array<string, mixed>
	 */
	private $locale;

	public function __construct(string $file)
	{
		ob_start();
		/** @psalm-suppress UnresolvableInclude */
		$this->locale = require_once $file;
		ob_end_clean();
	}

	/**
	 * @return array<string, string>
	 */
	public function i18n(): array
	{
		$i18n = [];

		if (array_key_exists('i18n', $this->locale)) {
			$i18n = $this->locale['i18n'];
		}

		return $i18n;
	}

	public function dateFormat(): string
	{
		if (!array_key_exists('date-format', $this->locale)) {
			throw new RuntimeException('missing "date-format"');
		}

		return $this->locale['date-format'];
	}
}
