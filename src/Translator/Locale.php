<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Translator;

interface Locale
{
	public function dateFormat(): string;

	/**
	 * @return array<string, string>
	 */
	public function i18n(): array;
}
