<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Reader;

use tomtomsen\ResumeGenerator\Models\Resume;

interface Reader
{
	public function read(): Resume;
}
