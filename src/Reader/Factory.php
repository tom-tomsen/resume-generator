<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Reader;

use InvalidArgumentException;
use SplFileInfo;
use tomtomsen\ResumeGenerator\Reader\JSON\Reader as JSONReader;

final class Factory
{
	private function __construct()
	{
	}

	public static function get(SplFileInfo $input): Reader
	{
		switch ($input->getExtension()) {
			case 'json':
				return new JSONReader($input);

			default:
				throw new InvalidArgumentException('no matching converter found (' . $input->getExtension() . ')');
		}
	}
}
