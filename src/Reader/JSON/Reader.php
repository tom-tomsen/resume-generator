<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Reader\JSON;

use InvalidArgumentException;
use JsonException;
use RuntimeException;
use SplFileInfo;
use tomtomsen\ResumeGenerator\Models\Certificate;
use tomtomsen\ResumeGenerator\Models\Certificate\Title as CertificateTitle;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;
use tomtomsen\ResumeGenerator\Models\Education;
use tomtomsen\ResumeGenerator\Models\Education\Title as EducationTitle;
use tomtomsen\ResumeGenerator\Models\Experience;
use tomtomsen\ResumeGenerator\Models\Experience\Technology as ExperienceTechnology;
use tomtomsen\ResumeGenerator\Models\Experience\Title as ExperienceTitle;
use tomtomsen\ResumeGenerator\Models\Institution;
use tomtomsen\ResumeGenerator\Models\Institution\Name as InstitutionName;
use tomtomsen\ResumeGenerator\Models\Location;
use tomtomsen\ResumeGenerator\Models\Organisation;
use tomtomsen\ResumeGenerator\Models\Organisation\Name as OrganisationName;
use tomtomsen\ResumeGenerator\Models\Person;
use tomtomsen\ResumeGenerator\Models\Person\Contact as PersonContact;
use tomtomsen\ResumeGenerator\Models\Person\Image as PersonImage;
use tomtomsen\ResumeGenerator\Models\Person\Name as PersonName;
use tomtomsen\ResumeGenerator\Models\Person\Title as PersonTitle;
use tomtomsen\ResumeGenerator\Models\Resume;
use tomtomsen\ResumeGenerator\Models\Url;
use tomtomsen\ResumeGenerator\Reader\Reader as BaseReader;
use function array_key_exists;
use function is_array;
use function is_string;
use function json_decode;
use function realpath;
use const JSON_THROW_ON_ERROR;

final class Reader implements BaseReader
{
	/**
	 * @var SplFileInfo
	 */
	private $file;

	public function __construct(SplFileInfo $file)
	{
		if (!$file->isReadable()) {
			throw new RuntimeException('File must be readable');
		}

		$this->file = $file;
	}

	public function read(): Resume
	{
		$file = $this->file->openFile('r');
		$content = '';

		while (!$file->eof()) {
			$content .= $file->fgets();
		}

		try {
			$data = json_decode($content, TRUE, 512, JSON_THROW_ON_ERROR);
		} catch (JsonException $ex) {
			throw new RuntimeException('Failed to parse json', 0, $ex);
		}

		return $this->parse($data);
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parse(array $data): Resume
	{
		if (!array_key_exists('person', $data)) {
			throw new InvalidArgumentException('missing field person in data');
		}

		if (!is_array($data['person'])) {
			throw new InvalidArgumentException('expected field person to be an array');
		}

		$person = $this->parsePerson($data['person']);
		$resume = new Resume($person);

		if (array_key_exists('certificates', $data) && is_array($data['certificates'])) {
			foreach ($data['certificates'] as $certificateData) {
				$certificate = $this->parseCertificate($certificateData);
				$resume->certificates()->add($certificate);
			}
		}

		if (array_key_exists('experience', $data) && is_array($data['experience'])) {
			foreach ($data['experience'] as $experienceData) {
				$experience = $this->parseExperience($experienceData);
				$resume->experiences()->add($experience);
			}
		}

		if (array_key_exists('education', $data) && is_array($data['education'])) {
			foreach ($data['education'] as $educationData) {
				$education = $this->parseEducation($educationData);
				$resume->educations()->add($education);
			}
		}

		return $resume;
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parsePerson(array $data): Person
	{
		if (!array_key_exists('name', $data)) {
			throw new InvalidArgumentException('missing name in person data');
		}

		if (!array_key_exists('title', $data)) {
			throw new InvalidArgumentException('missing title in person data');
		}

		$name = PersonName::fromString($data['name']);
		$title = PersonTitle::fromString($data['title']);

		$person = new Person($name, $title);

		if (array_key_exists('description', $data) && is_string($data['description'])) {
			$person->changeDescription($data['description']);
		}

		if (array_key_exists('image', $data) && is_string($data['image'])) {
			$isPathAbsolute = (realpath($data['image']) === $data['image']);

			$path = $data['image'];

			if (!$isPathAbsolute) {
				$path = $this->file->getPath() . '/' . $data['image'];
			}
			$person->changeImage(PersonImage::fromPath($path));
		}

		if (array_key_exists('contact', $data) && is_array($data['contact'])) {
			$contactData = $data['contact'];

			if (array_key_exists('address', $contactData)) {
				$person->contacts()->add(new PersonContact('address', $contactData['address']['label']));
			}

			if (array_key_exists('email', $contactData)) {
				$person->contacts()->add(new PersonContact('email', $contactData['email']['label']));
			}

			if (array_key_exists('phone', $contactData)) {
				$person->contacts()->add(new PersonContact('phone', $contactData['phone']['label']));
			}

			if (array_key_exists('github', $contactData)) {
				$person->contacts()->add(new PersonContact('github', $contactData['github']['label']));
			}

			if (array_key_exists('bitbucket', $contactData)) {
				$person->contacts()->add(new PersonContact('bitbucket', $contactData['bitbucket']['label']));
			}
		}

		return $person;
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parseCertificate(array $data): Certificate
	{
		if (!array_key_exists('title', $data)) {
			throw new InvalidArgumentException('expected title to be in data');
		}

		if (!array_key_exists('date', $data)) {
			throw new InvalidArgumentException('expected date in data');
		}

		$date = Date::fromString($data['date']);
		$title = CertificateTitle::fromString($data['title']);

		return new Certificate($title, $date);
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parseExperience(array $data): Experience
	{
		if (!array_key_exists('title', $data)) {
			throw new InvalidArgumentException('expected title to be in data');
		}

		if (!array_key_exists('organisation', $data)) {
			throw new InvalidArgumentException('expected organisation in data');
		}

		if (!array_key_exists('duration', $data)) {
			throw new InvalidArgumentException('expected duration in data');
		}

		$title = ExperienceTitle::fromString($data['title']);
		$duration = $this->parseDuration($data['duration']);
		$organisation = $this->parseOrganisation($data['organisation']);

		$experience = new Experience($title, $duration, $organisation);

		if (array_key_exists('description', $data)) {
			$experience->changeDescription($data['description']);
		}

		if (array_key_exists('technologies', $data)) {
			foreach ($data['technologies'] as $technology) {
				$experience->technologies()->add(ExperienceTechnology::fromString($technology));
			}
		}

		return $experience;
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parseDuration(array $data): Duration
	{
		if (!array_key_exists('start', $data)) {
			throw new InvalidArgumentException('expected start in data');
		}

		try {
			$start = Date::fromString($data['start']);
		} catch (InvalidArgumentException $ex) {
			throw new InvalidArgumentException('invalid start', 0, $ex);
		}

		$end = NULL;

		if (array_key_exists('end', $data)) {
			try {
				$end = Date::fromString($data['end']);
			} catch (InvalidArgumentException $ex) {
				throw new InvalidArgumentException('invalid end', 0, $ex);
			}
		}

		return new Duration($start, $end);
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parseOrganisation(array $data): Organisation
	{
		if (!array_key_exists('name', $data)) {
			throw new InvalidArgumentException('expected name to be in data');
		}

		if (!array_key_exists('location', $data)) {
			throw new InvalidArgumentException('expected location in data');
		}

		$name = OrganisationName::fromString($data['name']);
		$location = Location::fromString($data['location']);

		$organisation = new Organisation($name, $location);

		if (array_key_exists('url', $data)) {
			$organisation->assignUrl(Url::fromString($data['url']));
		}

		return $organisation;
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parseEducation(array $data): Education
	{
		if (!array_key_exists('title', $data)) {
			throw new InvalidArgumentException('expected title to be in data');
		}

		if (!array_key_exists('institution', $data)) {
			throw new InvalidArgumentException('expected institution in data');
		}

		if (!array_key_exists('duration', $data)) {
			throw new InvalidArgumentException('expected duration in data');
		}

		$title = EducationTitle::fromString($data['title']);
		$duration = $this->parseDuration($data['duration']);
		$institution = $this->parseInstitution($data['institution']);

		$education = new Education($title, $duration, $institution);

		if (array_key_exists('description', $data)) {
			$education->changeDescription($data['description']);
		}

		return $education;
	}

	/**
	 * @param array<mixed> $data
	 */
	private function parseInstitution(array $data): Institution
	{
		if (!array_key_exists('name', $data)) {
			throw new InvalidArgumentException('expected name to be in data');
		}

		if (!array_key_exists('location', $data)) {
			throw new InvalidArgumentException('expected location in data');
		}

		$name = InstitutionName::fromString($data['name']);
		$location = Location::fromString($data['location']);

		return new Institution($name, $location);
	}
}
