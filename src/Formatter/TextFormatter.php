<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Formatter;

use function array_values;
use function count;
use function explode;
use function implode;
use function mb_strlen;
use const PHP_EOL;

final class TextFormatter
{
	public function wrap(string $str, int $length, string $glue = PHP_EOL): string
	{
		$wordList = explode(' ', $str);

		for ($i = 0; $i + 1 < count($wordList);) {
			$nextWord = $wordList[$i + 1];

			if (mb_strlen($nextWord) + mb_strlen($wordList[$i]) + 1 > $length) {
				++$i;
			} else {
				$wordList[$i] = $wordList[$i] . ' ' . $nextWord;
				unset($wordList[$i + 1]);
				$wordList = array_values($wordList);
			}
		}

		return implode($glue, $wordList);
	}
}
