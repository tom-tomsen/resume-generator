<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Formatter;

use tomtomsen\ResumeGenerator\Models\Duration;
use tomtomsen\ResumeGenerator\Translator\Translator;

final class DurationFormatter
{
	/**
	 * @var DateFormatter
	 */
	private $dateFormatter;

	/**
	 * @var Translator
	 */
	private $translator;

	public function __construct(DateFormatter $dateFormatter, Translator $translator)
	{
		$this->dateFormatter = $dateFormatter;
		$this->translator = $translator;
	}

	public function format(Duration $duration): string
	{
		$startFormatted = $this->dateFormatter->format($duration->start());

		$end = $duration->end();

		if (!$end) {
			return $this->t(
				'since {start}',
				[
					'start' => $startFormatted,
				]
			);
		}

		$endFormatted = $this->dateFormatter->format($end);

		if ($startFormatted === $endFormatted) {
			return $this->t(
				'in {date}',
				[
					'date' => $startFormatted,
				]
			);
		}

		return $this->t(
			'{start} - {end}',
			[
				'start' => $startFormatted,
				'end' => $endFormatted,
			]
		);
	}

	/**
	 * @param array<string, string> $params
	 */
	private function t(string $text, array $params = []): string
	{
		return $this->translator->translate($text, $params);
	}
}
