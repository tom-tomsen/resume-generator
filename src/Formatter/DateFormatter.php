<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Formatter;

use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Translator\Translator;

final class DateFormatter
{
	/**
	 * @var string
	 */
	private $dateFormat;

	/**
	 * @var Translator
	 */
	private $translator;

	public function __construct(string $dateFormat, Translator $translator)
	{
		$this->dateFormat = $dateFormat;
		$this->translator = $translator;
	}

	public function format(Date $date): string
	{
		return $this->translator->translate(
			$date->format($this->dateFormat),
			[
				'January' => $this->translator->translate('January'),
				'February' => $this->translator->translate('February'),
				'March' => $this->translator->translate('March'),
				'April' => $this->translator->translate('April'),
				'May' => $this->translator->translate('May'),
				'June' => $this->translator->translate('June'),
				'July' => $this->translator->translate('July'),
				'August' => $this->translator->translate('August'),
				'September' => $this->translator->translate('September'),
				'October' => $this->translator->translate('October'),
				'November' => $this->translator->translate('November'),
				'December' => $this->translator->translate('December'),
			]
		);
	}
}
