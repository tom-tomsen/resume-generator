<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Converter\Markdown;

use tomtomsen\ResumeGenerator\Converter\Converter as BaseConverter;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Formatter\DurationFormatter;
use tomtomsen\ResumeGenerator\Formatter\TextFormatter;
use tomtomsen\ResumeGenerator\Models\Person\Contact;
use tomtomsen\ResumeGenerator\Models\Resume;
use tomtomsen\ResumeGenerator\Translator\Translator;
use function implode;
use function ltrim;
use function sprintf;
use const PHP_EOL;

final class Converter implements BaseConverter
{
	/**
	 * @var Resume
	 */
	private $resume;

	/**
	 * @var Translator
	 */
	private $translator;

	/**
	 * @var DateFormatter
	 */
	private $dateFormatter;

	/**
	 * @var DurationFormatter
	 */
	private $durationFormatter;

	/**
	 * @var TextFormatter
	 */
	private $textFormatter;

	public function __construct(
		Resume $resume,
		Translator $translator,
		DateFormatter $dateFormatter,
		DurationFormatter $durationFormatter,
		TextFormatter $textFormatter
	) {
		$this->resume = $resume;
		$this->translator = $translator;
		$this->dateFormatter = $dateFormatter;
		$this->durationFormatter = $durationFormatter;
		$this->textFormatter = $textFormatter;
	}

	public function toString(): string
	{
		$markdown = [];

		$markdown[] = '# ' . $this->resume->person()->name();
		$markdown[] = '';

		$markdown[] = $this->textFormatter->wrap($this->resume->person()->description(), 80, PHP_EOL);
		$markdown[] = '';

		$contacts = $this->resume->person()->contacts();

		if (!$contacts->isEmpty()) {
			$markdown[] = '## ' . $this->t('Contact');
			$markdown[] = '';

			foreach ($contacts as $contact) {
				$markdown[] = '- ' . $this->contactAsMarkdown($contact);
			}
			$markdown[] = '';
		}

		$educations = $this->resume->educations();

		if (!$educations->isEmpty()) {
			$markdown[] = '## ' . $this->t('Education');

			foreach ($educations as $education) {
				$markdown[] = '';
				$markdown[] = '### ' . (string) $education->title();
				$markdown[] = '';
				$markdown[] = (string) $this->durationFormatter->format($education->duration());
				$markdown[] = '';
				$markdown[] = $this->textFormatter->wrap((string) $education->description(), 80, PHP_EOL);
			}
			$markdown[] = '';
		}

		$experiences = $this->resume->experiences();

		if (!$experiences->isEmpty()) {
			$markdown[] = '## ' . $this->t('Work Experience');

			foreach ($experiences as $experience) {
				$markdown[] = '';
				$markdown[] = '### ' . (string) $experience->title();
				$markdown[] = '';
				$markdown[] = (string) $this->durationFormatter->format($experience->duration());
				$markdown[] = '';
				$markdown[] = $this->textFormatter->wrap((string) $experience->description(), 80, PHP_EOL);
			}
			$markdown[] = '';
		}

		$certificates = $this->resume->certificates();

		if (!$certificates->isEmpty()) {
			$markdown[] = '## ' . $this->t('Certificates');
			$markdown[] = '';

			foreach ($certificates as $certificate) {
				$markdown[] = '- ' . $this->dateFormatter->format($certificate->date()) . ' - ' . (string) $certificate->title();
			}
			$markdown[] = '';
		}

		return implode(PHP_EOL, $markdown);
	}

	private function contactAsMarkdown(Contact $contact): string
	{
		switch ($contact->type()) {
			case 'email':
				return $this->link($contact->contact(), 'mailto:' . $contact->contact());

			case 'github':
				$link = 'https://www.github.com/' . ltrim($contact->contact(), '/');

				return $this->link($link, $link);

			case 'bitbucket':
				$link = 'https://www.bitbucket.org/' . ltrim($contact->contact(), '/');

				return $this->link($link, $link);

			case 'phone':
			case 'address':
			default:
				return (string) $contact->contact();
		}
	}

	private function link(string $label, string $link): string
	{
		return sprintf('[%s](%s)', $label, $link);
	}

	private function t(string $text): string
	{
		return $this->translator->translate($text);
	}
}
