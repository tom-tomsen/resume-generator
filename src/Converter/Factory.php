<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Converter;

use InvalidArgumentException;
use SplFileInfo;
use tomtomsen\ResumeGenerator\Converter\Markdown\Converter as MarkdownConverter;
use tomtomsen\ResumeGenerator\Converter\PDF\Converter as PDFConverter;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Formatter\DurationFormatter;
use tomtomsen\ResumeGenerator\Formatter\TextFormatter;
use tomtomsen\ResumeGenerator\Models\Resume;
use tomtomsen\ResumeGenerator\Translator\Translator;

final class Factory
{
	private function __construct()
	{
	}

	public static function get(
		SplFileInfo $output,
		Resume $resume,
		Translator $translator,
		DateFormatter $dateFormatter,
		DurationFormatter $durationFormatter,
		TextFormatter $textFormatter
	): Converter {
		switch ($output->getExtension()) {
			case 'md':
				return new MarkdownConverter($resume, $translator, $dateFormatter, $durationFormatter, $textFormatter);

			case 'pdf':
				return new PDFConverter($resume, $translator, $dateFormatter, $durationFormatter);

			default:
				throw new InvalidArgumentException('no matching converter found');
		}
	}
}
