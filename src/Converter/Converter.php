<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Converter;

interface Converter
{
	public function toString(): string;
}
