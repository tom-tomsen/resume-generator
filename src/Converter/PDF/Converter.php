<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Converter\PDF;

use tomtomsen\ResumeGenerator\Converter\Converter as BaseConverter;
use tomtomsen\ResumeGenerator\Formatter\DateFormatter;
use tomtomsen\ResumeGenerator\Formatter\DurationFormatter;
use tomtomsen\ResumeGenerator\Models\Certificate;
use tomtomsen\ResumeGenerator\Models\Date;
use tomtomsen\ResumeGenerator\Models\Duration;
use tomtomsen\ResumeGenerator\Models\Education;
use tomtomsen\ResumeGenerator\Models\Experience;
use tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection;
use tomtomsen\ResumeGenerator\Models\Person\Contact;
use tomtomsen\ResumeGenerator\Models\Resume;
use tomtomsen\ResumeGenerator\Translator\Translator;
use function define;
use function ltrim;
use function mb_strlen;
use function ob_get_clean;
use function ob_start;
use const PHP_EOL;

define('FPDF_FONTPATH', __DIR__ . '/fonts');

final class Converter implements BaseConverter
{
	public const TEXT_COLOR_DARK = [50, 50, 50];

	public const TEXT_COLOR_LIGHT = [125, 125, 125];

	/**
	 * @var Resume
	 */
	private $resume;

	/**
	 * @var Translator
	 */
	private $translator;

	/**
	 * @var DateFormatter
	 */
	private $dateFormatter;

	/**
	 * @var DurationFormatter
	 */
	private $durationFormatter;

	public function __construct(
		Resume $resume,
		Translator $translator,
		DateFormatter $dateFormatter,
		DurationFormatter $durationFormatter
	) {
		$this->resume = $resume;
		$this->translator = $translator;
		$this->dateFormatter = $dateFormatter;
		$this->durationFormatter = $durationFormatter;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public function toString(): string
	{
		$pdf = $this->generatePdf();

		ob_start();
		$pdf->Output();

		return (string) ob_get_clean();
	}

	private function generatePdf(): PDF
	{
		$pdf = new PDF(PDF::ORIENTATION_PORTRAIT, PDF::SIZE_A4);
		$pdf->SetMargins(0, 0);

		$w = 190;
		$m = ($pdf->GetPageWidth() - $w) / 2;

		// $pdf->SetMargins($m, $m);
		$pdf->SetCompression(TRUE);
		$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetLineHeight(5);

		$contacts = $this->resume->person()->contacts();
		$email = '';

		if ($contacts->has('email')) {
			$email = ' <' . $contacts->get('email')[0] . '>';
		}

		$pdf->SetAuthor($this->resume->person()->name() . $email);
		$pdf->SetCreator('Resume Generator by tomtomsen');
		$pdf->SetKeywords($this->resume->person()->name() . ' resume cv lebenslauf');
		$pdf->SetTitle('Resume of ' . $this->resume->person()->name());
		$pdf->SetSubject('Resume of ' . $this->resume->person()->name());
		$pdf->SetDisplayMode('fullwidth', 'two');

		$pdf->AddFont('calibri', '', 'calibri.ttf', TRUE);
		$pdf->AddFont('calibri-bold', '', 'calibri-bold.ttf', TRUE);
		$pdf->AddFont('icon-font', '', 'icon-font.ttf', TRUE);
		$pdf->AddFont('leelawui', '', 'leelawui.ttf', TRUE);
		$pdf->AddFont('leelawuisl', '', 'leelawuisl.ttf', TRUE);
		$pdf->AddFont('roboto-black', '', 'roboto-black.ttf', TRUE);
		$pdf->AddFont('roboto-bold', '', 'roboto-bold.ttf', TRUE);
		$pdf->AddFont('roboto-light-italic', '', 'roboto-light-italic.ttf', TRUE);

		$pdf->AddPage();
		$this->pageIndicator($pdf, 1, 2, $pdf->GetPageWidth() - 10, 3, 20);
		$this->header($pdf, $m, $m, $w);
		$this->intro($pdf, $m, $pdf->GetY(), $w);
		$this->experience($pdf, $m, $pdf->GetY(), $w);

		$pdf->AddPage();
		$this->pageIndicator($pdf, 2, 2, $pdf->GetPageWidth() - 10, 3, 20);
		$this->header($pdf, $m, $m, $w);
		$this->education($pdf, $m, $pdf->GetY(), $w);
		$this->certifications($pdf, $m, $pdf->GetY(), $w);
		$this->footer($pdf, 2, 2, 1, -20, $pdf->GetPageWidth() - 2);

		$pdf->Close();

		return $pdf;
	}

	private function pageIndicator(PDF $pdf, float $n, float $of, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetTextColorAsArray(self::TEXT_COLOR_LIGHT);

		for ($i = 0; $of - $n > $i; ++$i) {
			$icon = 'c';
			$pdf->SetFont('icon-font', '', 5);
			$width = $pdf->GetStringWidth($icon);
			$pdf->Cell($width, $pdf->getLineHeight(), $icon, $border = 0, $ln = 0);
		}

		for ($i = 0; $i < $n; ++$i) {
			$icon = 'd';
			$pdf->SetFont('icon-font', '', 5);
			$width = $pdf->GetStringWidth($icon);
			$pdf->Cell($width, $pdf->getLineHeight(), $icon, $border = 0, $ln = 0);
		}
	}

	private function intro(PDF $pdf, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$description = $this->resume->person()->description();

		if (0 !== mb_strlen($description)) {
			$pdf->SetFont('leelawui', '', 9);
			$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

			$pdf->WordWrap($description, $x, $y, $w);

			// $this->p($pdf, $description, $x, $y, $w);

			$pdf->SetY($pdf->GetY() + 5);
		}
	}

	private function footer(PDF $pdf, int $i, int $n, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('leelawui', '', 5);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$footnote =
		'This document was generated automatically through a continuous deployment pipeline.' . PHP_EOL .
		'Quality was ensured through various linting, static analysis and testing tools.' . PHP_EOL .
		'Source code is available on https://bitbucket.org/tom-tomsen/resume-generator';

		$pdf->MultiCell($w = $w, $h = 3, $footnote, $border = 0, $align = 'C', $fill = FALSE);
	}

	/**
	 * | <logo> | <title> | <info>
	 * |   50   |   90    |   50.
	 */
	private function header(PDF $pdf, float $x, float $y, float $w): void
	{
		$y = $y + 5; // top margin

		$col1 = $w / 4.75;
		$col3 = $w / 3;
		$col2 = $w - ($col1 + $col3);

		// left
		$imgW = $col1 / 1.33;
		$image = $this->resume->person()->image();

		if ($image) {
			$pdf->Image($image->path(), $x + (($col1 - $imgW) / 2), $y, $imgW, $imgW, 'PNG');
		}

		// center
		$this->title($pdf, (string) $this->resume->person()->name(), $x + $col1, $y + 5, $col2);
		$this->subtitle($pdf, (string) $this->resume->person()->title(), $x + $col1, $pdf->GetY(), $col2);

		$this->contacts($pdf, $x + $col1 + $col2, $y, $col3);

		$pdf->SetY($y + 40);
	}

	private function contacts(PDF $pdf, float $x, float $y, float $w): void
	{
		// right
		$pdf->SetFont('calibri', '', 9);
		$pdf->SetY($y);
		$pdf->SetX($x);

		foreach ($this->resume->person()->contacts() as $contact) {
			$this->icon($pdf, $this->contactIconLetter($contact->type()), $x, $pdf->GetY() + 5, 6, 5);

			$pdf->SetFont('calibri', '', 9);
			$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

			$this->contactCell($pdf, $contact, $pdf->GetX(), $pdf->GetY(), $w - 5);
		}
	}

	private function experience(PDF $pdf, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$this->h2($pdf, $this->t('Work Experience'), $x, $y);

		foreach ($this->resume->experiences() as $experience) {
			$this->experienceRow($pdf, $experience, $x, $pdf->GetY(), $w);
		}
	}

	private function education(PDF $pdf, float $x, float $y, float $w): void
	{
		$this->h2($pdf, $this->t('Education'), $x, $y);

		foreach ($this->resume->educations() as $education) {
			$this->educationRow($pdf, $education, $x, $pdf->getY(), $w);
		}
	}

	private function certifications(PDF $pdf, float $x, float $y, float $w): void
	{
		$this->h2($pdf, $this->t('Certificates'), $x, $y);

		foreach ($this->resume->certificates() as $cert) {
			$this->certificateRow($pdf, $cert, $x, $pdf->GetY(), $w);
		}
	}

	/**
	 * | <left> | <right> |
	 * |   65   |    125  |.
	 */
	private function experienceRow(PDF $pdf, Experience $experience, float $x, float $y, float $w): void
	{
		$col1 = $w / 3;
		$col2 = $w - ($w / 3);

		// left col
		$this->organisation($pdf, (string) $experience->organisation()->name(), (string) $experience->organisation()->location(), $x, $y, $col1);
		$this->duration($pdf, $experience->duration(), $x, $pdf->GetY(), $col1);

		$url = $experience->organisation()->url();

		if ($url) {
			$this->organisationUrl($pdf, (string) $url, $x, $pdf->GetY() + 1, $col1);
		}

		// right col
		$this->jobTitle($pdf, (string) $experience->title(), $x + $col1, $y, $w);
		$this->technologies($pdf, $experience->technologies(), $x + $col1, $pdf->GetY(), $col2);

		$description = $experience->description();

		if (0 < mb_strlen($description)) {
			$this->description($pdf, $experience->description(), $x + $col1, $pdf->GetY(), $col2);
		}

		$pdf->SetY($pdf->GetY() + 5);
	}

	private function educationRow(PDF $pdf, Education $education, float $x, float $y, float $w): void
	{
		$col1 = $w / 3;
		$col2 = $w - ($w / 3);

		// left col
		$this->organisation($pdf, (string) $education->institution()->name(), (string) $education->institution()->location(), $x, $y, $col1);
		$this->duration($pdf, $education->duration(), $x, $pdf->GetY(), $col1);

		// right col
		$this->jobTitle($pdf, (string) $education->title(), $x + $col1, $y, $col2);
		$this->description($pdf, $education->description(), $x + $col1, $pdf->GetY(), $col2);

		$pdf->SetY($pdf->GetY() + 5);
	}

	private function certificateRow(PDF $pdf, Certificate $cert, float $x, float $y, float $w): void
	{
		$col1 = $w / 3;
		$col2 = $w - ($w / 3);

		// left col
		$this->date($pdf, $cert->date(), $x, $y, $col1);

		// right col
		$this->description($pdf, (string) $cert->title(), $x + $col1, $y, $col2);
	}

	private function title(PDF $pdf, string $text, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('roboto-bold', '', 25);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$textWidth = $pdf->GetStringWidth($text);
		$pdf->Cell(
			$w = $textWidth,
			$h = 10,
			$text,
			$border = 0,
			$ln = 1,
			$align = 'L',
			$fill = FALSE
		);
	}

	private function subtitle(PDF $pdf, string $text, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('roboto-light-italic', '', 12);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$width = $pdf->GetStringWidth($text);
		$pdf->Cell(
			$w = $width,
			$h = $pdf->getLineHeight(),
			$text,
			$border = 0,
			$ln = 1,
			$align = 'L',
			$fill = FALSE
		);
	}

	private function h2(PDF $pdf, string $text, float $x, float $y): void
	{
		$y += 5;

		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('Roboto-Black', '', 16);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$pdf->Cell($w = 0, $h = $pdf->GetLineHeight(), $text, $border = 0, $ln = 1, $align = 'L', $fill = FALSE);

		$pdf->SetY($y + 10);
	}

	private function p(PDF $pdf, string $text, float $x, float $y, float $w): void
	{
		$pdf->MultiCell(
			$w = $w,
			$h = $pdf->GetLineHeight(),
			$text,
			$border = 0,
			$align = 'L',
			$fill = FALSE
		);
	}

	private function iconB(PDF $pdf, string $icon, float $x, float $y, float $w, float $h): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('icon-font', '', '9');
		$iconWidth = $pdf->GetStringWidth($icon);
		$pdf->Cell($w = $w, $h = $h, $icon, $border = 0, $ln = 0, $align = 'C', $fill = FALSE);
	}

	private function icon(PDF $pdf, string $icon, float $x, float $y, float $w, float $h): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('icon-font', '', '9');
		$iconWidth = $pdf->GetStringWidth($icon);
		$pdf->Cell($w = $w, $h = $h, $icon, $border = 0, $ln = 0, $align = 'C', $fill = FALSE);
	}

	private function organisation(PDF $pdf, string $name, string $location, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('calibri-bold', '', 11);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$organizationWidth = $pdf->GetStringWidth($name);
		$pdf->Cell($w = $organizationWidth, $h = $pdf->GetLineHeight(), $name, $border = 0, $ln = 0, $align = 'L', $fill = FALSE);

		$location = ', ' . $location;
		$locationWidth = $pdf->GetStringWidth($location);
		$pdf->Cell($w = $locationWidth, $h = $pdf->GetLineHeight(), $location, $border = 0, $ln = 1, $align = 'L', $fill = FALSE);
	}

	private function duration(PDF $pdf, Duration $duration, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('leelawui', '', 9);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$durationStr = $this->durationFormatter->format($duration);

		$durationWidth = $pdf->GetStringWidth($durationStr);
		$pdf->Cell($w = $durationWidth, $h = $pdf->GetLineHeight(), $durationStr, $border = 0, $ln = 1, $align = 'L', $fill = FALSE);
	}

	private function date(PDF $pdf, Date $date, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('leelawui', '', 9);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$dateStr = $this->dateFormatter->format($date);
		$dateWidth = $pdf->GetStringWidth($this->t($dateStr));
		$pdf->Cell($w = $dateWidth, $h = $pdf->GetLineHeight(), $dateStr, $border = 0, $ln = 1, $align = 'L', $fill = FALSE);
	}

	private function link(PDF $pdf, string $url, string $label, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$urlWidth = $pdf->GetStringWidth($label);
		$pdf->Cell($w = $urlWidth, $h = $pdf->GetLineHeight(), $label, $border = 0, $ln = 0, $align = 'L', $fill = FALSE, $link = $url);

		$pdf->SetFont('icon-font', '', 3);

		$y = $pdf->GetY();
		$x = $pdf->GetX();

		$pdf->SetY($y - .5);
		$pdf->SetX($x + .1);
		$pdf->Cell($w = 10, $h = $pdf->GetLineHeight(), 'x', $border = 0, $ln = 0, $align = 'L', $fill = FALSE);
	}

	private function organisationUrl(PDF $pdf, string $url, float $x, float $y, float $w): void
	{
		$pdf->SetFont('leelawui', '', 8);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_LIGHT);

		$this->link($pdf, $url, $url, $x, $y, $w);
	}

	private function jobTitle(PDF $pdf, string $title, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('calibri-bold', '', 11);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$pdf->Cell($w = $w, $h = $pdf->GetLineHeight(), $title, $border = 0, $ln = 2, $align = 'L', $fill = FALSE);
	}

	private function description(PDF $pdf, string $description, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('leelawui', '', 9);
		$pdf->SetTextColorAsArray(self::TEXT_COLOR_DARK);

		$this->p($pdf, $description, $x, $y, $w);
	}

	private function technologies(PDF $pdf, TechnologyCollection $technologies, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$pdf->SetFont('leelawuisl', '', 9);

		$technologyStr = '';

		foreach ($technologies as $idx => $technology) {
			if (0 < $idx) {
				$technologyStr .= ', ' . $technology;
			} else {
				$technologyStr = $technology;
			}
		}
		$pdf->MultiCell($w = $w, $h = $pdf->GetLineHeight(), $technologyStr, $border = 0, $align = 'L', $fill = FALSE);
	}

	private function contactIconLetter(string $type): string
	{
		switch ($type) {
			case 'address': return 'l';

			case 'phone': return 'o';

			case 'email': return 'm';

			case 'github': return 'g';

			case 'bitbucket': return 'b';
		}

		return ' ';
	}

	private function contactCell(PDF $pdf, Contact $contact, float $x, float $y, float $w): void
	{
		$pdf->SetY($y);
		$pdf->SetX($x);

		$link = NULL;

		switch ($contact->type()) {
			case 'email':
				$link = 'mailto:' . $contact->contact();

				break;

			case 'github':
				$link = 'https://www.github.com/' . ltrim($contact->contact(), '/');

				break;

			case 'bitbucket':
				$link = 'https://www.bitbucket.org/' . ltrim($contact->contact(), '/');

				break;
		}

		if ($link) {
			$this->link($pdf, $link, $contact->contact(), $pdf->GetX(), $pdf->GetY(), $w);
		} else {
			$pdf->Cell($w, $pdf->getLineHeight(), $contact->contact(), $border = 0, $ln = 0, $align = 'L', $fill = FALSE);
		}
	}

	/**
	 * @param array<string, string> $params
	 */
	private function t(string $text, array $params = []): string
	{
		return $this->translator->translate($text, $params);
	}
}
