<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Converter\PDF;

use tFPDF;
use function assert;
use function count;
use function explode;
use function is_array;
use function preg_split;
use function trim;
use const PREG_SPLIT_DELIM_CAPTURE;

final class PDF extends tFPDF
{
	public const ORIENTATION_PORTRAIT = 'P';

	public const SIZE_A4 = 'A4';

	/**
	 * @var float
	 */
	private $lineHeight;

	public function __construct(string $orientation, string $size)
	{
		parent::__construct($orientation, 'mm', $size);

		$this->lineHeight = 6;
	}

	public function GetPageWidth(): float
	{
		return $this->w;
	}

	public function GetPageHeight(): float
	{
		return $this->h;
	}

	/**
	 * @param array<float> $color
	 */
	public function SetTextColorAsArray(array $color): void
	{
		assert(3 === count($color));

		parent::SetTextColor($color[0], $color[1], $color[2]);
	}

	public function SetLineHeight(float $h): void
	{
		$this->lineHeight = $h;
	}

	public function GetLineHeight(): float
	{
		return $this->lineHeight;
	}

	public function WordWrap(string $text, float $x, float $y, float $maxWidth): void
	{
		$a = preg_split('/\*\*(.*)\*\*/U', $text, -1, PREG_SPLIT_DELIM_CAPTURE);

		if (!is_array($a)) {
			return;
		}

		$words = [];

		foreach ($a as $i => $p) {
			if (0 === $i % 2) {
				// normal
				foreach (explode(' ', trim($p)) as $w) {
					$words[] = [$w, ''];
				}
			} else {
				// bold
				foreach (explode(' ', trim($p)) as $w) {
					$words[] = [$w, 'B'];
				}
			}
		}

		$partialText = '';
		$partialTextWidth = 0;
		$fullWidth = 0;

		$this->SetY($y);
		$this->SetX($x);

		$currentStyle = '';

		foreach ($words as $word) {
			if ('' === $word[0]) {
				continue;
			}

			switch ($word[1]) {
				case '':
					if ('' !== $currentStyle && 0 < $partialTextWidth) {
						$this->Cell($partialTextWidth, $this->getLineHeight(), $partialText, $border = 0, $ln = 0);
						$partialText = '';
						$partialTextWidth = 0;
					}
					$this->SetFont('leelawui', '', 9);
					$currentStyle = '';

				break;

				case 'B':
					if ('B' !== $currentStyle && 0 < $partialTextWidth) {
						$this->Cell($partialTextWidth, $this->getLineHeight(), $partialText, $border = 0, $ln = 0);
						$partialText = '';
						$partialTextWidth = 0;
					}
					$this->SetFont('calibri-bold', '', 9);
					$currentStyle = 'B';

				break;
			}
			$spaceWidth = $this->GetStringWidth(' ');
			$wordWidth = $this->GetStringWidth($word[0]);

			if ($fullWidth + $wordWidth > $maxWidth) {
				$this->Cell($partialTextWidth, $this->getLineHeight(), $partialText, $border = 0, $ln = 1);
				$this->SetX($x);

				$partialText = '';
				$partialTextWidth = 0;
				$fullWidth = 0;
			}

			$partialText .= $word[0] . ' ';
			$partialTextWidth += $wordWidth + $spaceWidth;
			$fullWidth += $wordWidth + $spaceWidth;
		}

		if ('' !== trim($partialText)) {
			$this->Cell($partialTextWidth, $this->getLineHeight(), $partialText, $border = 0, $ln = 1);
			$this->SetX($x);
		}
	}
}
