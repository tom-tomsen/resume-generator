<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Education;

use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use tomtomsen\ResumeGenerator\Models\Education;
use Traversable;
use function array_key_exists;
use function count;

/**
 * @implements IteratorAggregate<int, Education>
 */
final class EducationCollection implements Countable, IteratorAggregate
{
	/**
	 * @var array<Education>
	 */
	private $list;

	public function __construct()
	{
		$this->list = [];
	}

	public function add(Education $Education): void
	{
		$this->list[] = $Education;
	}

	public function get(int $index): Education
	{
		if (!array_key_exists($index, $this->list)) {
			throw new InvalidArgumentException(); // TODO(tom.tomsen): use outofbounds
		}

		return $this->list[$index];
	}

	/**
	 * @return Traversable<int, Education>
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->list);
	}

	public function isEmpty(): bool
	{
		return empty($this->list);
	}

	public function count(): int
	{
		return count($this->list);
	}
}
