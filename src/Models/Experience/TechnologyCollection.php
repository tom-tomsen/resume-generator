<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Experience;

use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use Traversable;
use function array_key_exists;
use function count;

/**
 * @implements IteratorAggregate<int, Technology>
 */
final class TechnologyCollection implements Countable, IteratorAggregate
{
	/**
	 * @var array<Technology>
	 */
	private $list;

	public function __construct()
	{
		$this->list = [];
	}

	public function add(Technology $Technology): void
	{
		$this->list[] = $Technology;
	}

	public function get(int $index): Technology
	{
		if (!array_key_exists($index, $this->list)) {
			throw new InvalidArgumentException(); // TODO(tom.tomsen): use outofbounds
		}

		return $this->list[$index];
	}

	/**
	 * @return Traversable<int, Technology>
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->list);
	}

	public function count(): int
	{
		return count($this->list);
	}
}
