<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Experience;

use InvalidArgumentException;
use function trim;

final class Technology
{
	/**
	 * @var string
	 */
	private $technology;

	private function __construct(string $technology)
	{
		$technology = trim($technology);

		if ('' === $technology) {
			throw new InvalidArgumentException('technology is empty');
		}

		$this->technology = $technology;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public static function fromString(string $str): self
	{
		return new self($str);
	}

	public function toString(): string
	{
		return $this->technology;
	}
}
