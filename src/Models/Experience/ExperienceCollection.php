<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Experience;

use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use tomtomsen\ResumeGenerator\Models\Experience;
use Traversable;
use function array_key_exists;
use function count;

/**
 * @implements IteratorAggregate<int, Experience>
 */
final class ExperienceCollection implements Countable, IteratorAggregate
{
	/**
	 * @var array<Experience>
	 */
	private $list;

	public function __construct()
	{
		$this->list = [];
	}

	public function add(Experience $Experience): void
	{
		$this->list[] = $Experience;
	}

	public function get(int $index): Experience
	{
		if (!array_key_exists($index, $this->list)) {
			throw new InvalidArgumentException(); // TODO(tom.tomsen): use outofbounds
		}

		return $this->list[$index];
	}

	/**
	 * @return Traversable<int, Experience>
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->list);
	}

	public function isEmpty(): bool
	{
		return empty($this->list);
	}

	public function count(): int
	{
		return count($this->list);
	}
}
