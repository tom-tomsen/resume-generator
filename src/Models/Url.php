<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use InvalidArgumentException;
use function in_array;
use function parse_url;
use const PHP_URL_SCHEME;

final class Url
{
	/**
	 * @var string
	 */
	private $url;

	private function __construct(string $url)
	{
		$scheme = parse_url($url, PHP_URL_SCHEME);

		if (NULL === $scheme || !in_array($scheme, self::allowedUrlSchemes(), TRUE)) {
			throw new InvalidArgumentException('invalid url');
		}

		$this->url = $url;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public static function fromString(string $url): self
	{
		return new self($url);
	}

	public function toString(): string
	{
		return $this->url;
	}

	/**
	 * @return array<string>
	 */
	public static function allowedUrlSchemes(): array
	{
		return [
			'http',
			'https',
			'mailto',
		];
	}
}
