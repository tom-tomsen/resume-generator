<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use InvalidArgumentException;

final class Duration
{
	/**
	 * @var Date
	 */
	private $start;

	/**
	 * @var ?Date
	 */
	private $end;

	public function __construct(Date $start, ?Date $end = NULL)
	{
		if ($end && 0 < $start->equals($end)) {
			throw new InvalidArgumentException('start expected to be before end');
		}

		$this->start = $start;
		$this->end = $end;
	}

	public function start(): Date
	{
		return $this->start;
	}

	public function end(): ?Date
	{
		return $this->end;
	}

	public function hasEnd(): bool
	{
		return NULL !== $this->end;
	}
}
