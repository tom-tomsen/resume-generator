<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Person\ContactCollection;
use tomtomsen\ResumeGenerator\Models\Person\Image;
use tomtomsen\ResumeGenerator\Models\Person\Name;
use tomtomsen\ResumeGenerator\Models\Person\Title;
use function trim;

final class Person
{
	/**
	 * @var ?Image
	 */
	private $image;

	/**
	 * @var Name
	 */
	private $name;

	/**
	 * @var Title
	 */
	private $title;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var ContactCollection
	 */
	private $contacts;

	public function __construct(Name $name, Title $title)
	{
		$this->name = $name;
		$this->title = $title;
		$this->contacts = new ContactCollection();
		$this->image = NULL;
		$this->description = '';
	}

	public function name(): Name
	{
		return $this->name;
	}

	public function title(): Title
	{
		return $this->title;
	}

	public function image(): ?Image
	{
		return $this->image;
	}

	public function description(): string
	{
		return $this->description;
	}

	public function contacts(): ContactCollection
	{
		return $this->contacts;
	}

	public function changeDescription(string $description): void
	{
		$this->description = trim($description);
	}

	public function changeImage(Image $image): void
	{
		$this->image = $image;
	}
}
