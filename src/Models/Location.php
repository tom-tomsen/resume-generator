<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use InvalidArgumentException;
use function trim;

final class Location
{
	/**
	 * @var string
	 */
	private $location;

	private function __construct(string $location)
	{
		$location = trim($location);

		if ('' === $location) {
			throw new InvalidArgumentException('location was empty');
		}

		$this->location = $location;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public static function fromString(string $location): self
	{
		return new self($location);
	}

	public function toString(): string
	{
		return $this->location;
	}
}
