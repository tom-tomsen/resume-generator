<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Certificate\Title;

final class Certificate
{
	/**
	 * @var Date
	 */
	private $date;

	/**
	 * @var Title
	 */
	private $title;

	public function __construct(Title $title, Date $date)
	{
		$this->date = $date;
		$this->title = $title;
	}

	public function date(): Date
	{
		return $this->date;
	}

	public function title(): Title
	{
		return $this->title;
	}
}
