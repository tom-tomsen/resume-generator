<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Institution\Name;

final class Institution
{
	/**
	 * @var Name
	 */
	private $name;

	/**
	 * @var Location
	 */
	private $location;

	public function __construct(Name $name, Location $location)
	{
		$this->name = $name;
		$this->location = $location;
	}

	public function name(): Name
	{
		return $this->name;
	}

	public function location(): Location
	{
		return $this->location;
	}
}
