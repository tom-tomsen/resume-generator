<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Certificate\CertificateCollection;
use tomtomsen\ResumeGenerator\Models\Education\EducationCollection;
use tomtomsen\ResumeGenerator\Models\Experience\ExperienceCollection;

final class Resume
{
	/**
	 * @var Person
	 */
	private $person;

	/**
	 * @var EducationCollection
	 */
	private $educations;

	/**
	 * @var ExperienceCollection
	 */
	private $experiences;

	/**
	 * @var CertificateCollection
	 */
	private $certs;

	public function __construct(Person $person)
	{
		$this->person = $person;
		$this->educations = new EducationCollection();
		$this->experiences = new ExperienceCollection();
		$this->certs = new CertificateCollection();
	}

	public function person(): Person
	{
		return $this->person;
	}

	public function educations(): EducationCollection
	{
		return $this->educations;
	}

	public function experiences(): ExperienceCollection
	{
		return $this->experiences;
	}

	public function certificates(): CertificateCollection
	{
		return $this->certs;
	}
}
