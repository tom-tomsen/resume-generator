<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Experience\TechnologyCollection;
use tomtomsen\ResumeGenerator\Models\Experience\Title;
use function trim;

final class Experience
{
	/**
	 * @var Title
	 */
	private $title;

	/**
	 * @var Duration
	 */
	private $duration;

	/**
	 * @var Organisation
	 */
	private $organisation;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var TechnologyCollection
	 */
	private $technologies;

	public function __construct(Title $title, Duration $duration, Organisation $organisation)
	{
		$this->title = $title;
		$this->duration = $duration;
		$this->organisation = $organisation;
		$this->technologies = new TechnologyCollection();
		$this->description = '';
	}

	public function title(): Title
	{
		return $this->title;
	}

	public function duration(): Duration
	{
		return $this->duration;
	}

	public function description(): string
	{
		return $this->description;
	}

	public function organisation(): Organisation
	{
		return $this->organisation;
	}

	public function technologies(): TechnologyCollection
	{
		return $this->technologies;
	}

	public function changeDescription(string $description): void
	{
		$description = trim($description);

		$this->description = $description;
	}
}
