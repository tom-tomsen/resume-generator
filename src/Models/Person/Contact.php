<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Person;

use InvalidArgumentException;
use function trim;

final class Contact
{
	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $contact;

	public function __construct(string $type, string $contact)
	{
		$contact = trim($contact);
		$type = trim($type);

		if ('' === $contact) {
			throw new InvalidArgumentException('contact was empty');
		}

		if ('' === $type) {
			throw new InvalidArgumentException('type was empty');
		}

		$this->type = $type;
		$this->contact = $contact;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public function type(): string
	{
		return $this->type;
	}

	public function contact(): string
	{
		return $this->contact;
	}

	public function toString(): string
	{
		return $this->contact();
	}
}
