<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Person;

use InvalidArgumentException;
use function trim;

final class Name
{
	/**
	 * @var string
	 */
	private $name;

	private function __construct(string $name)
	{
		$name = trim($name);

		if ('' === $name) {
			throw new InvalidArgumentException('name was empty');
		}

		$this->name = $name;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public static function fromString(string $name): self
	{
		return new self($name);
	}

	public function toString(): string
	{
		return $this->name;
	}
}
