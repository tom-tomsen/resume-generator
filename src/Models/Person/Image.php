<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Person;

use InvalidArgumentException;
use function file_exists;

final class Image
{
	/**
	 * @var string
	 */
	private $path;

	private function __construct(string $path)
	{
		if (!file_exists($path)) {
			throw new InvalidArgumentException("image '{$path}' not found");
		}

		$this->path = $path;
	}

	public static function fromPath(string $path): self
	{
		return new self($path);
	}

	public function path(): string
	{
		return $this->path;
	}
}
