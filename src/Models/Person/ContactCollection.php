<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Person;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;
use function array_key_exists;
use function count;

/**
 * @implements IteratorAggregate<int, Contact>
 */
final class ContactCollection implements Countable, IteratorAggregate
{
	/**
	 * @var array<Contact>
	 */
	private $list;

	/**
	 * @var array<array<Contact>>
	 */
	private $types;

	public function __construct()
	{
		$this->list = [];
		$this->types = [];
	}

	public function add(Contact $contact): void
	{
		$this->list[] = $contact;

		$type = $contact->type();

		$this->types[$type][] = $contact;
	}

	public function has(string $type): bool
	{
		return array_key_exists($type, $this->types) && !empty($this->types[$type]);
	}

	/**
	 * @return array<Contact>
	 */
	public function get(string $type): array
	{
		if (!array_key_exists($type, $this->types)) {
			return [];
		}

		return $this->types[$type];
	}

	/**
	 * @return Traversable<int, Contact>
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->list);
	}

	public function isEmpty(): bool
	{
		return empty($this->list);
	}

	public function count(): int
	{
		return count($this->list);
	}
}
