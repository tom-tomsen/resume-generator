<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Education\Title;
use function trim;

final class Education
{
	/**
	 * @var Title
	 */
	private $title;

	/**
	 * @var Duration
	 */
	private $duration;

	/**
	 * @var Institution
	 */
	private $institution;

	/**
	 * @var string
	 */
	private $description;

	public function __construct(Title $title, Duration $duration, Institution $institution)
	{
		$this->title = $title;
		$this->duration = $duration;
		$this->institution = $institution;

		$this->description = '';
	}

	public function title(): Title
	{
		return $this->title;
	}

	public function description(): string
	{
		return $this->description;
	}

	public function institution(): Institution
	{
		return $this->institution;
	}

	public function duration(): Duration
	{
		return $this->duration;
	}

	public function changeDescription(string $description): void
	{
		$description = trim($description);

		$this->description = $description;
	}
}
