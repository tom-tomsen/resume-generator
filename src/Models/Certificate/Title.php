<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Certificate;

use InvalidArgumentException;
use function trim;

final class Title
{
	/**
	 * @var string
	 */
	private $title;

	private function __construct(string $title)
	{
		$title = trim($title);

		if ('' === $title) {
			throw new InvalidArgumentException('title was empty');
		}

		$this->title = $title;
	}

	public function __toString(): string
	{
		return $this->toString();
	}

	public static function fromString(string $title): self
	{
		return new self($title);
	}

	public function toString(): string
	{
		return $this->title;
	}
}
