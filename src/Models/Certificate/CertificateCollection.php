<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models\Certificate;

use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use tomtomsen\ResumeGenerator\Models\Certificate;
use Traversable;
use function array_key_exists;
use function count;

/**
 * @implements IteratorAggregate<int, Certificate>
 */
final class CertificateCollection implements Countable, IteratorAggregate
{
	/**
	 * @var array<Certificate>
	 */
	private $list;

	public function __construct()
	{
		$this->list = [];
	}

	public function add(Certificate $Certificate): void
	{
		$this->list[] = $Certificate;
	}

	public function get(int $index): Certificate
	{
		if (!array_key_exists($index, $this->list)) {
			throw new InvalidArgumentException(); // TODO(tom.tomsen): use outofbounds
		}

		return $this->list[$index];
	}

	/**
	 * @return Traversable<int, Certificate>
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->list);
	}

	public function isEmpty(): bool
	{
		return empty($this->list);
	}

	public function count(): int
	{
		return count($this->list);
	}
}
