<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use DateTimeImmutable;
use InvalidArgumentException;

final class Date
{
	/**
	 * @var DateTimeImmutable
	 */
	private $date;

	public function __construct(DateTimeImmutable $date)
	{
		$this->date = $date;
	}

	public static function fromString(string $str, string $format = 'm-Y'): self
	{
		$date = DateTimeImmutable::createFromFormat($format, $str);

		if (FALSE === $date) {
			throw new InvalidArgumentException('date expected to be in format ' . $format);
		}

		return new self($date);
	}

	public function format(string $format): string
	{
		return $this->date->format($format);
	}

	public function date(): DateTimeImmutable
	{
		return $this->date;
	}

	public function equals(self $other): int
	{
		return $this <=> $other;
	}
}
