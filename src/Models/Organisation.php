<?php

declare(strict_types=1);

namespace tomtomsen\ResumeGenerator\Models;

use tomtomsen\ResumeGenerator\Models\Organisation\Name;

final class Organisation
{
	/**
	 * @var Name
	 */
	private $name;

	/**
	 * @var Location
	 */
	private $location;

	/**
	 * @var ?Url
	 */
	private $url;

	public function __construct(Name $name, Location $location)
	{
		$this->name = $name;
		$this->location = $location;
		$this->url = NULL;
	}

	public function name(): Name
	{
		return $this->name;
	}

	public function location(): Location
	{
		return $this->location;
	}

	public function assignUrl(Url $url): void
	{
		$this->url = $url;
	}

	public function url(): ?Url
	{
		return $this->url;
	}
}
